<?php

namespace Drupal\hfc_catalog_helper;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;

/**
 * Defines the Catalog Content Import Service.
 */
class CatalogContentImportService {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Module configuration settings name.
   */
  const MODULE_SETTINGS = 'hfc_catalog_helper.settings';

  /**
   * Import configuration settings name.
   *
   * @var string
   */
  const IMPORT_SETTINGS = 'hfc_catalog_helper.import_settings';

  /**
   * Stores the Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Stores the Date Formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  private $dateFormatter;

  /**
   * Stores the Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Stores the GuzzleHttp Client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The Date Formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP Client.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TimeInterface $time,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    ClientInterface $http_client,
  ) {
    $this->configFactory = $config_factory;
    $this->time = $time;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
  }

  /**
   * Import legacy content.
   *
   * @param string $type
   *   The content type to import.
   * @param array $options
   *   An associative array of options whose values
   *   come from cli, aliases, config, etc.
   */
  public function import(string $type, array $options): void {

    // Exit unless the specified content type is explicitly allowed here.
    if (!in_array($type, ['funding_proposal'])) {
      $this->getLogger('hfc_catalog_helper')->error(
        'Invalid content type @type requested for import.',
        ['@type' => $type]
      );
      return;
    }

    // Limit the number of items to import.
    $limit = intval($options['limit'] ?? 0);
    if ($limit === 0) {
      $limit = $this->getSettings('content_import_limit') ?? 500;
    }

    // Get an index of nodes to import.
    $index = $this->getIndex($type);
    $count = 0;

    foreach ($index as $item) {

      $target = $this->checkExisting($item);

      if (empty($options['force']) && !empty($target) && $target->changed->value >= $item->changed) {
        continue;
      }

      if ($this->importContent($item->nid, $target)) {
        $count++;
        usleep(50000);
      }

      if ($count >= $limit) {
        break;
      }
    }

    $this->getLogger('hfc_catalog_helper')->info($this->t(
      'Imported @count @type items.',
      ['@count' => $count, '@type' => $type]
    ));
  }

  /**
   * Import a funding proposal node.
   */
  public function importContent(int $nid, object $target = NULL): int {

    $json_data = $this->getContent($nid);

    if (empty($json_data)) {
      return 0;
    }

    $source = json_decode($json_data);

    if (empty($target)) {
      $values = $this->getBaseValues($source);

      // Override content type.
      $values['type'] = 'funding_proposal';
      $values['uuid'] = $source->uuid;

      $target = $this->entityTypeManager->getStorage('node')->create($values);
    }

    $target->title->setValue($source->title);
    $target->field_funding_cost->setValue($source->field_cost->und[0]->value);
    $target->field_effective_term->target_id = $source->field_term->und[0]->value;
    $target->field_funding_status->setValue($source->field_note->und[0]->value ?? '');

    if (!empty($source->field_proposal)) {
      $this->addFiles($target->field_funding_proposal, $source->field_proposal, 'tif-proposals');
    }

    if (!empty($source->field_board_report)) {
      $this->addFiles($target->field_funding_board_report, $source->field_board_report, 'tif-proposals');
    }

    if (!empty($source->field_final_report)) {
      $this->addFiles($target->field_funding_final_report, $source->field_final_report, 'tif-proposals');
    }

    $target->setNewRevision(TRUE);
    $target->setRevisionUserId('303');
    $target->setRevisionCreationTime($source->revision_timestamp ?? $source->changed);
    $target->changed = $source->changed;

    $target->save();

    return $target->id() ?? 0;
  }

  /**
   * Get import settings.
   *
   * @param string $key
   *   The settings key to retrieve.
   */
  private function getSettings(string $key) {
    $settings = $this->configFactory->get(static::IMPORT_SETTINGS);
    return $settings->get($key) ?? NULL;
  }

  /**
   * Get index of content to import.
   */
  private function getIndex(string $type): array {

    $url = $this->getSettings('source_uri');
    $endpoint = "$url/node/$type/index";

    $response = $this->httpClient->request('GET', $endpoint, [
      RequestOptions::TIMEOUT => 60,
      RequestOptions::HEADERS => [
        'Apikey' => $this->getSettings('apikey'),
      ],
    ]);

    if ($response->getStatusCode() == 200) {
      $body = $response->getBody();
      return (array) json_decode($body);
    }
    else {
      $this->getLogger('hfc_catalog_helper')->error(
        'Source @endpoint API response @status: @message.',
        [
          '@endpoint' => $endpoint,
          '@status' => $response->getStatusCode(),
          '@message' => $response->getReasonPhrase(),
        ]
      );
      return [];
    }
  }

  /**
   * Get source content from node ID.
   */
  public function getContent(int $nid): ?string {

    $url = $this->getSettings('source_uri');
    $endpoint = "$url/node/$nid";

    $response = $this->httpClient->request('GET', $endpoint, [
      RequestOptions::TIMEOUT => 60,
      RequestOptions::HEADERS => [
        'Apikey' => $this->getSettings('apikey'),
      ],
    ]);

    if ($response->getStatusCode() == 200) {
      return $response->getBody();
    }
    else {
      $this->getLogger('hfc_catalog_helper')->error(
        'Source @endpoint API response @status: @message.',
        [
          '@endpoint' => $endpoint,
          '@status' => $response->getStatusCode(),
          '@message' => $response->getReasonPhrase(),
        ]
      );
      return NULL;
    }
  }

  /**
   * Add files to field.
   */
  private function addFiles(object $field, object $incoming, string $path): void {

    foreach ($incoming->{LanguageInterface::LANGCODE_NOT_SPECIFIED} as $item) {

      if ($file = $this->getFile($item, "{$path}")) {
        $values = [
          'target_id' => $file->id(),
          'display' => 1,
        ];

        foreach (['alt', 'title'] as $metadata) {
          if (!empty($item->{$metadata})) {
            $values[$metadata] = $item->{$metadata};
          }
        }

        // Check against duplicate values and exit if found.
        foreach ($field as $field_value) {
          if ($field_value->target_id == $file->id()) {
            return;
          }
        }
        $field->appendItem($values);
      }
    }
  }

  /**
   * Get the specified file.
   */
  private function getFile($source, $target): ?object {

    if (!file_exists("public://{$target}")) {
      mkdir("public://{$target}", 0775, TRUE);
    }

    $target_uri = "public://{$target}/{$source->filename}";

    $storage = $this->entityTypeManager->getStorage('file');

    $existing = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('uri', $target_uri)
      ->execute();

    if (!empty($existing)) {
      $existing = reset($existing);
      return $storage->load($existing);
    }

    if (!file_exists($target_uri)) {
      $download = system_retrieve_file($source->uri, $target_uri, TRUE, FileSystemInterface::EXISTS_REPLACE);

      if (is_object($download)) {
        return $download;
      }
    }

    return NULL;
  }

  /**
   * Check existing content by timestamp.
   *
   * @param object $item
   *   The source index object to check.
   *
   * @return bool
   *   Returns true if content with timestamp is found.
   */
  private function checkExisting(object $item): ?NodeInterface {

    $result = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(FALSE)
      ->condition('uuid', $item->uuid)
      ->execute();

    if ($result) {
      $nid = reset($result);
      return $this->entityTypeManager->getStorage('node')->load($nid);
    }

    return NULL;
  }

  /**
   * Get base target node values from the source data.
   */
  private function getBaseValues(object $source): array {
    return [
      'type' => $source->type,
      'created' => $source->created,
      'changed' => $source->changed,
      'revisions' => 1,
      'status' => 1,
      'language' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'uid' => $this->getSettings('default_owner') ?? 0,
    ];
  }

}
