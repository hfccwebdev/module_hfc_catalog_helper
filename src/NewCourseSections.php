<?php

namespace Drupal\hfc_catalog_helper;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\hankdata\Entity\HankTerm;
use Drupal\hankdata\HankDataInterface;
use Drupal\hfc_hank_api\HfcHankApiInterface;

/**
 * Class NewCourseSections.
 *
 * New Course Sections.
 */
class NewCourseSections implements NewCourseSectionsInterface {
  use StringTranslationTrait;

  /**
   * Define the pattern for undergrad academic terms.
   */
  const UG_TERM_PATTERN = '^[0-9]./(FA|WI|SP|SU)$';

  /**
   * Number of days to be included.
   */
  const DAYS = 14;

  /**
   * Define the target URL for Colleague Self-Service Course listings.
   *
   * Example: https://sss.hfcc.edu/Student/Courses/Search?keyword=CIS-100
   */
  const HFC_SELF_SERVICE = 'https://sss.hfcc.edu/Student/Courses/Search';

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\hankdata\HankDataInterface definition.
   *
   * @var \Drupal\hankdata\HankDataInterface
   */
  protected $hankdata;

  /**
   * Drupal\hfc_hank_api\HfcHankApiInterface definition.
   *
   * @var \Drupal\hfc_hank_api\HfcHankApiInterface
   */
  protected $hankApi;

  /**
   * Drupal\hfc_catalog_helper\CatalogUtilitiesInterface definition.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Stores an array of academic terms ids.
   *
   * @var string[]
   */
  protected $terms;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Constructs a new NewCourseSections object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The Date Formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager service.
   * @param \Drupal\hankdata\HankDataInterface $hankdata
   *   The HANK Data service.
   * @param \Drupal\hfc_hank_api\HfcHankApiInterface $hfc_hank_api
   *   The HANK API service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $hfc_catalog_helper
   *   The HANK Catalog helper service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    HankDataInterface $hankdata,
    HfcHankApiInterface $hfc_hank_api,
    CatalogUtilitiesInterface $hfc_catalog_helper,
    TimeInterface $time
  ) {
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->hankdata = $hankdata;
    $this->hankApi = $hfc_hank_api;
    $this->helper = $hfc_catalog_helper;
    $this->time = $time;
    $this->buildTerms();
  }

  /**
   * Build the array of Academic terms.
   */
  private function buildTerms() {
    $query = $this->entityTypeManager->getStorage('hank_term')->getQuery()
      ->accessCheck(FALSE)
      ->condition('term_reg_start_date', $this->time->getRequestTime(), '<=')
      ->condition('term_add_end_date', $this->time->getRequestTime(), '>=')
      ->condition('terms_id', self::UG_TERM_PATTERN, 'REGEXP')
      ->sort('term_start_date');
    $this->terms = $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getSubjectOpts() {
    return $this->hankdata->getHankSubjectOpts();
  }

  /**
   * {@inheritdoc}
   */
  public function getTermOpts() {
    return $this->terms;
  }

  /**
   * {@inheritdoc}
   */
  public function view($options = []) {

    $terms = !empty($options['term']) ? [$options['term']] : array_keys($this->terms);

    if (!empty($terms)) {
      $output = [];
      foreach ($terms as $terms_id) {
        $term = HankTerm::load($terms_id);
        $tid = str_replace("/", "", $terms_id);

        $sections = $this->hankApi->getData("course-sections-open-seats", $tid, [], TRUE);
        usort($sections, [$this, "sortSecStatusDesc"]);

        $rows = [];

        foreach (array_slice($sections, 0, 100) as $key => $sec) {
          if (
            $sec->SEC_STATUS_DATE_UX > $this->time->getRequestTime() - 86400 * self::DAYS &&
            (empty($options['course_subject']) || ($sec->SEC_SUBJECT == mb_strtoupper($options['course_subject']))) &&
            (empty($options['course_number']) || ($sec->SEC_COURSE_NO == mb_strtoupper($options['course_number']))) &&
            $sec->SEC_STATUS == "A"
          ) {

            $display_date = $this->dateFormatter->format($sec->SEC_STATUS_DATE_UX, 'custom', 'l, n/j/y');

            if ($sec->SEC_WAITLIST_COUNT > 0) {
              $sec_full = $this->t('WAITLIST');
            }
            elseif ($sec->SEC_SEATS_TAKEN >= $sec->SEC_CAPACITY) {
              $sec_full = $this->t('FULL');
            }
            else {
              $sec_full = FALSE;
            }

            $crs_title = !empty($sec->SEC_SHORT_TITLE) ? $sec->SEC_SHORT_TITLE : 'ERROR';
            $crs_name = mb_strtolower($sec->SEC_SUBJECT . '-' . $sec->SEC_COURSE_NO);
            $urlopts = [
              'query' => ['keyword' => mb_strtoupper($sec->SEC_SUBJECT . '-' . $sec->SEC_COURSE_NO)],
              'attributes' => ['target' => '_blank'],
            ];
            $url = Url::fromUri(self::HFC_SELF_SERVICE, $urlopts);
            $rows[] = [
              $sec->SEC_SUBJECT . '-' . $sec->SEC_COURSE_NO . '-' . $sec->SEC_NO,
              !empty($url) ? Link::fromTextAndUrl($crs_title, $url) : $crs_title,
              $display_date,
              !empty($sec->SEC_MEETING_DAYS) ? str_replace('_', '', $sec->SEC_MEETING_DAYS) : $this->t('TBD'),
              !empty($sec->SEC_START_TIME) ? $sec->SEC_START_TIME . ' - ' . $sec->SEC_END_TIME : NULL,
              !empty($sec->SEC_ROOM) ? $sec->SEC_ROOM : NULL,
              !empty($sec->SEC_FACULTY_NAME) ? $sec->SEC_FACULTY_NAME : $this->t('TBD'),
              $sec_full ? [
                'data' => $sec_full,
                'class' => ['sec-full'],
              ] : [
                'data' => $this->t('OPEN'),
                'class' => ['sec-open'],
              ],
            ];
          }
        }

        $output[] = [
          '#prefix' => '<h2 class="title">',
          '#type' => 'markup',
          '#markup' => $this->t('Sections added to @label', ['@label' => $term->label()]),
          '#suffix' => '</h2>',
        ];

        $output[] = [
          '#type' => 'table',
          '#header' => [
            $this->t('Section'),
            $this->t('Course Title'),
            $this->t('Date Added'),
            $this->t('Days'),
            $this->t('Time'),
            $this->t('Room'),
            $this->t('Faculty'),
            $this->t('Availability'),
          ],
          '#rows' => $rows,
          '#attributes' => ['class' => ['recently-added-sections']],
          '#sticky' => TRUE,
          '#empty' => $this->t('No new sections matching this search were created in the past two weeks.'),
        ];
      }
      return $output;
    }
    else {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('There are currently no terms with open registration.'),
      ];
    }
  }

  /**
   * Callback for descending sort by sec_status_date.
   */
  private function sortSecStatusDesc($a, $b) {
    return strcmp(
      $b->SEC_STATUS_DATE_UX . $a->SEC_SUBJECT . $a->SEC_COURSE_NO . $a->SEC_NO,
      $a->SEC_STATUS_DATE_UX . $b->SEC_SUBJECT . $b->SEC_COURSE_NO . $b->SEC_NO
    );
  }

}
