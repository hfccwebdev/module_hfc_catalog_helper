<?php

namespace Drupal\hfc_catalog_helper;

/**
 * Defines the Catalog Archive Builder Interface.
 *
 * @package Drupal\hfc_catalog_helper
 */
interface CatalogArchiveBuilderInterface {

  /**
   * Generate Catalog output.
   *
   * @param string $catalog_year
   *   The Catalog Year to archive.
   * @param string $base_path
   *   The base path for file export.
   * @param bool[] $options
   *   An array of option flags to control process flow.
   *     -  nc: No Courses.
   *     -  np: No Programs.
   *     -  nd: No Employee Directory.
   *     -  ns: No Program Sequences.
   *
   * @return bool
   *   Returns TRUE if no errors were encountered.
   */
  public function generate($catalog_year, $base_path, array $options = []);

}
