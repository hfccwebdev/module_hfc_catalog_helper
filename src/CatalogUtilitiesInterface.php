<?php

namespace Drupal\hfc_catalog_helper;

use Drupal\node\NodeInterface;

/**
 * Interface CatalogUtilitiesInterface.
 *
 * Utilities for Catalog.
 *
 * @ingroup hfcc_modules
 */
interface CatalogUtilitiesInterface {

  /**
   * Select all node titles and nids of a specified type.
   *
   * @param string $type
   *   The desired content type.
   *
   * @return array
   *   An array keyed by nid with the title as value.
   */
  public function getNodeTitlesByType($type);

  /**
   * Query all nodes of a given type.
   *
   * @param string $type
   *   The node type to query.
   * @param string[] $conditions
   *   An array of optional conditions for filtering.
   *   Array item keys should be field, value, and (optionally) operator.
   *
   * @return int[]
   *   An array of selected node IDs.
   */
  public function getNidsByType(string $type, array $conditions = []): ?array;

  /**
   * Determine if course or program master is inactive.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to check.
   *
   * @return bool
   *   True if course master is inactive.
   */
  public function isInactiveMaster(NodeInterface $node);

  /**
   * Find an existing course with the same subject and number.
   *
   * @param string $subject
   *   Course subject to search.
   * @param string $number
   *   Course number to search.
   * @param string $type
   *   Content type (optional)
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of information about existing courses.
   */
  public function findMatchingCourseInfo($subject, $number, $type = NULL);

  /**
   * Check for existing course with same subject and number.
   *
   * @param string $subject
   *   Course subject.
   * @param string $number
   *   Course number.
   *
   * @return array
   *   An array of information about matches found.
   */
  public function findMatchingCourses(string $subject, string $number): array;

  /**
   * Calculate default contact hours based on provided credit hours.
   *
   * @param float $credit_hours
   *   Credit hours.
   *
   * @return float
   *   Contact hours.
   */
  public function getDefaultContactHours(float $credit_hours): float;

  /**
   * Get Supplemental Course Info from Course Master nid.
   *
   * @param int|Node $master
   *   The Node ID of the Course Master or the Course Master object.
   *
   * @return Node
   *   The Supplemental Course info
   */
  public function getSupplementalCourseInfo($master);

  /**
   * Get Catalog Course by Course Master nid.
   *
   * @param int|Node $master
   *   The Node ID of the Course Master or the Course Master object.
   *
   * @return Node
   *   The Catalog Course node.
   */
  public function getCatalogCourse($master);

  /**
   * Get Active Proposal by Course Master nid.
   *
   * @param int|Node $master
   *   The Node ID of the Course Master or the Course Master object.
   *
   * @return Node
   *   The Course Proposal node.
   */
  public function getActiveCourseProposal($master);

  /**
   * Get an array of valid HFC Degree Types.
   *
   * @return string[]
   *   An array of options.
   */
  public function getDegreeTypes();

  /**
   * Get an array of schools values.
   *
   * @return string[]
   *   An array of options.
   */
  public function getSchools();

  /**
   * Get Supplemental Program Info from Program Master nid.
   *
   * @param int|Node $master
   *   The Node ID of the Program Master or the Program Master object.
   *
   * @return Node
   *   The Supplemental Program info
   */
  public function getSupplementalProgramInfo($master);

  /**
   * Get Catalog Program by Program Master nid.
   *
   * @param int|Node $master
   *   The Node ID of the Program Master or the Program Master object.
   *
   * @return Node
   *   The Catalog Program node.
   */
  public function getCatalogProgram($master);

  /**
   * Get Active Proposal by Program Master nid.
   *
   * @param int|Node $master
   *   The Node ID of the Program Master or the Program Master object.
   *
   * @return Node
   *   The Program Proposal node.
   */
  public function getActiveProgramProposal($master);

  /**
   * Get Course by Subject and Number from the HFC HANK API.
   *
   * @param string $name
   *   The course mnemonic and number, separated by a hyphen.
   *
   * @return \StdClass
   *   An object describing the HANK course.
   */
  public function getHankApiCourse($name);

  /**
   * Get an array of Instructional Methods from the HFC HANK API.
   *
   * @param bool $show_key
   *   Controls whether or not to include the key in the display.
   *
   * @return string[]
   *   Array of Instructional Methods.
   */
  public function getInstrMethods($show_key);

  /**
   * Get an array of academic levels.
   *
   * @return string[]
   *   An array of options.
   */
  public function getAcademicLevels();

  /**
   * Get renderable array of Articulation Agreements related to target program.
   *
   * This method should iterate through related pseudo programs to find
   * agreements attached to those as well.
   *
   * @param \Drupal\node\NodeInterface $target
   *   The target program for the search.
   *
   * @return array
   *   A renderable array.
   */
  public function getRelatedArticulationAgreements(NodeInterface $target);

  /**
   * Create a new node of the specified content type.
   *
   * @param string $type
   *   The desired content type.
   *
   * @return \Drupal\node\NodeInterface
   *   A new node.
   */
  public function makeNewContent(string $type): NodeInterface;

}
