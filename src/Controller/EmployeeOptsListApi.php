<?php

namespace Drupal\hfc_catalog_helper\Controller;

/**
 * Defines the Employee Options List API.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class EmployeeOptsListApi extends EmployeeDirectoryApi {

  /**
   * Build the query.
   */
  protected function buildQuery() {
    $query = parent::buildQuery();
    $query->addField('h', 'first_name');
    $query->addField('h', 'last_name');
    return $query;
  }

  /**
   * Build the output array.
   */
  protected function buildOutput(&$output, $employees) {
    foreach ($employees as $item) {
      $fullname = !empty($item->first_name) ? $item->first_name : '';
      $fullname .= !empty($item->last_name) ? ' ' . $item->last_name : '';
      $output[$item->hrper_id] = trim($fullname) . ' (' . $item->hrper_id . ')';
    }
  }

}
