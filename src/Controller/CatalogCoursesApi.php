<?php

namespace Drupal\hfc_catalog_helper\Controller;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Utility\UnroutedUrlAssemblerInterface;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines the Catalog Courses Api.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class CatalogCoursesApi extends CatalogApiBaseController {

  /**
   * Drupal\Core\Utility\UnroutedUrlAssemblerInterface definition.
   *
   * @var \Drupal\Core\Utility\UnroutedUrlAssemblerInterface
   */
  protected $urlAssembler;

  /**
   * Drupal\path_alias\AliasManagerInterface definition.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  private $aliasManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('request_stack'),
      $container->get('date.formatter'),
      $container->get('hfc_catalog_helper'),
      $container->get('unrouted_url_assembler'),
      $container->get('path_alias.manager')
    );
  }

  /**
   * Constructs a new Controller object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The active database connection.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The Symfony request stack.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The Date Formatter service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $catalog_helper
   *   The HFC Catalog Helper tools.
   * @param \Drupal\Core\Utility\UnroutedUrlAssemblerInterface $unrouted_url_assembler
   *   The URL Assembler service.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The Path Alias Manager service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    Connection $database,
    RequestStack $request_stack,
    DateFormatter $date_formatter,
    CatalogUtilitiesInterface $catalog_helper,
    UnroutedUrlAssemblerInterface $unrouted_url_assembler,
    AliasManagerInterface $alias_manager
  ) {
    parent::__construct($entity_type_manager, $database, $request_stack, $date_formatter, $catalog_helper);
    $this->urlAssembler = $unrouted_url_assembler;
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildQuery() {

    $query = $this->database->select('node', 'n');

    $query->join('node_field_data', 'd', "n.vid = d.vid");
    $query->join('node__field_crs_subject', 'field_crs_subject', "n.vid = field_crs_subject.revision_id");
    $query->join('node__field_crs_number', 'field_crs_number', "n.vid = field_crs_number.revision_id");
    $query->join('node__field_crs_title', 'field_crs_title', "n.vid = field_crs_title.revision_id");
    $query->join('node__field_crs_description', 'field_crs_description', "n.vid = field_crs_description.revision_id");
    $query->join('node__field_crs_credit_hours', 'field_crs_credit_hours', "n.vid = field_crs_credit_hours.revision_id");
    $query->join('node__field_acad_level', 'field_acad_level', "n.vid = field_acad_level.revision_id");

    $query->leftJoin('node__field_crs_short_title', 'field_crs_short_title', "n.vid = field_crs_short_title.revision_id");
    $query->leftJoin('node__field_crs_prq', 'field_crs_prq', "n.vid = field_crs_prq.revision_id");
    $query->leftJoin('node__field_crs_crq', 'field_crs_crq', "n.vid = field_crs_crq.revision_id");
    $query->leftJoin('node__field_inactive', 'i', "n.vid = i.revision_id");
    $query->leftJoin('node__field_effective_term', 'field_effective_term', "n.vid = field_effective_term.revision_id");

    $query->fields('n', ['nid']);
    $query->addField('field_acad_level', 'field_acad_level_value', 'acad_level');
    $query->addField('field_crs_subject', 'field_crs_subject_target_id', 'subject');
    $query->addField('field_crs_number', 'field_crs_number_value', 'number');
    $query->addField('field_crs_title', 'field_crs_title_value', 'title');
    $query->addField('field_crs_short_title', 'field_crs_short_title_value', 'short_title');
    $query->addField('field_crs_description', 'field_crs_description_value', 'description');
    $query->addField('field_crs_prq', 'field_crs_prq_value', 'prerequisites');
    $query->addField('field_crs_crq', 'field_crs_crq_value', 'corequisites');
    $query->addField('field_crs_credit_hours', 'field_crs_credit_hours_value', 'credit_hours');
    $query->addField('field_effective_term', 'field_effective_term_target_id', 'effective_term');

    $query->orderBy('d.title');

    $query->condition('d.type', 'catalog_course', '=');
    $query->condition('i.field_inactive_value', 1, "<>");

    if (!empty($this->args['nid'])) {
      $query->condition('n.nid', $this->args['nid'], '=');
    }

    if (!empty($this->args['subject'])) {
      $query->condition('field_crs_subject.field_crs_subject_target_id', mb_strtoupper($this->args['subject']), '=');
    }

    if (!empty($this->args['number'])) {
      $query->condition('field_crs_number.field_crs_number_value', $this->args['number'], '=');
    }

    if (!empty($this->args['acad_level'])) {
      $query->condition('field_acad_level.field_acad_level_value', $this->args['acad_level'], '=');
    }

    if (!empty($this->args['effective'])) {
      $query->isNull('field_effective_term.field_effective_term_target_id');
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildOutput(&$output, $nodes) {
    foreach ($nodes as $node) {
      $node->credit_hours = number_format($node->credit_hours, 2);
      $node->path = $this->buildPath($node->nid);
      $output[mb_strtolower("{$node->subject}-{$node->number}")] = $node;
    }
  }

  /**
   * Gets the path alias from the node ID.
   *
   * @param int $nid
   *   The node ID.
   *
   * @return string
   *   The fqdn path alias.
   */
  private function buildPath(int $nid) {
    $uri = "base:" . $this->aliasManager->getAliasByPath("/node/$nid");
    return $this->urlAssembler->assemble($uri, ["absolute" => TRUE]);
  }

}
