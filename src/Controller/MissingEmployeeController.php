<?php

namespace Drupal\hfc_catalog_helper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\hfc_hank_api\HfcHankApi;
use Drupal\hfc_catalog_helper\DataIntegrityTools;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Finds the employees missing from HANK.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class MissingEmployeeController extends ControllerBase {

  /**
   * Drupal\hfc_hank_api\HfcHankApi definition.
   *
   * @var \Drupal\hfc_hank_api\HfcHankApi
   */
  private $hankApi;

  /**
   * Drupal\hfc_catalog_helper\DataIntegrityTools definition.
   *
   * @var \Drupal\hfc_catalog_helper\DataIntegrityTools
   */
  private $dataIntegrityTools;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hfc_hank_api'),
      $container->get('data_integrity_tools')
    );
  }

  /**
   * Creates a ProposalsTabController object.
   *
   * @param \Drupal\hfc_hank_api\HfcHankApi $hfc_hank_api
   *   The HFC HANK API service.
   * @param \Drupal\hfc_catalog_helper\DataIntegrityTools $dataIntegrityTools
   *   The Data Integrity Tools service.
   */
  public function __construct(
    HfcHankApi $hfc_hank_api,
    DataIntegrityTools $dataIntegrityTools
  ) {
    $this->hankApi = $hfc_hank_api;
    $this->dataIntegrityTools = $dataIntegrityTools;
  }

  /**
   * Gets list of missing employee office and program lists.
   *
   * Implements getEmployeeRow().
   *
   * @return array
   *   $output
   */
  public function view() {
    $headers = [
      $this->t('Title'),
      $this->t('First Name'),
      $this->t('Last Name'),
      '',
    ];
    $missing = $this->dataIntegrityTools->staffdirMissingReferences();
    $offices = $missing['office'];
    $programs = $missing['program'];

    $output[] = [
      ['#markup' => $this->t('<h2>Missing Employees</h2>')],
    ];

    if ($offices) {
      $output[] = [
        ['#markup' => $this->t('<h3>Missing Office Administrators</h3>')],
      ];
      $output[] = [
        '#theme' => 'table',
        '#header' => $headers,
        '#rows' => array_map([$this, 'getEmployeeRow'], $offices),
      ];
    }
    else {
      $output['none_found'] = ['#markup' => $this->t('<p>No missing office administrators found.</p>')];
    }

    if ($programs) {
      $output[] = [
        ['#markup' => $this->t('<h3>Missing Program Administrators</h3>')],
      ];
      $output[] = [
        '#theme' => 'table',
        '#header' => $headers,
        '#rows' => array_map([$this, 'getEmployeeRow'], $programs),
      ];
    }
    else {
      $output['none_found'] = ['#markup' => $this->t('<p>No missing program administrators found.</p>')];
    }

    $output['#cache'] = ['max-age' => 0];
    return $output;
  }

  /**
   * Get employee and node info.
   */
  private function getEmployeeRow(object $missing) {
    $id = str_pad($missing->tid, 7, "0", STR_PAD_LEFT);
    $node_link = Link::createFromRoute($missing->title, 'entity.node.canonical', ['node' => $missing->nid]);
    $identity = $this->hankApi->getData('hank-identity-info', $id);
    $employee = $this->hankApi->getData('getpersoninfo-employee', $id);
    $employeeInfo['identity'] = !empty($identity) ? reset($identity) : NULL;
    $employeeInfo['employee'] = !empty($employee) ? reset($employee) : NULL;

    if (!empty($employeeInfo['employee']->HRP_EFFECT_RETIRE_DATE)) {
      $remarks = $this->t('Retired @date', ['@date' => $employeeInfo['employee']->HRP_EFFECT_RETIRE_DATE]);
    }
    elseif (!empty($employeeInfo['employee']->HRP_EFFECT_TERM_DATE)) {
      $remarks = $this->t('Employment ended @date', ['@date' => $employeeInfo['employee']->HRP_EFFECT_TERM_DATE]);
    }
    elseif (!empty($employeeInfo['employee']->POS_TITLE)) {
      $remarks = $employeeInfo['employee']->POS_TITLE;
    }
    else {
      $remarks = [];
    }

    return [
      $node_link,
      !empty($employeeInfo['identity']) ? $employeeInfo['identity']->H19_IDV_FIRST_NAME : $this->t('Unknown'),
      !empty($employeeInfo['identity']) ? $employeeInfo['identity']->H19_IDV_LAST_NAME : $this->t('Unknown'),
      $remarks,
    ];
  }

}
