<?php

namespace Drupal\hfc_catalog_helper\Controller;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the HANK Departments API.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class HankDepartmentsApi extends CatalogApiBaseController {

  /**
   * {@inheritdoc}
   */
  protected function buildQuery() {
    $query = $this->database->select('hank_depts', 'd');
    $query->fields('d', ['depts_id']);
    $query->condition('d.depts_active_flag', 'A');
    $query->orderBy('d.depts_id');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildOutput(&$output, $data) {
    $entity_storage = $this->entityTypeManager->getStorage('hank_dept');
    foreach ($data as $item) {
      $entity = $entity_storage->load($item->depts_id);
      $output[$entity->id()] = $this->formatOutputRow($entity);
    }
  }

  /**
   * Format output row.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to format.
   *
   * @return string[]
   *   An array of field values.
   */
  private function formatOutputRow(EntityInterface $entity) {

    $office = $entity->get('field_office')->entity ?? NULL;

    return [
      'depts_id' => $entity->id(),
      'depts_desc' => $entity->label(),
      'depts_school' => $entity->get('depts_school')->value,
      'url' => !empty($office) ? $office->field_website->uri : NULL,
      'depts_head_id' => $entity->get('depts_head_id')->value,
      'office_id' => !empty($office) ? $office->id() : NULL,
    ];
  }

}
