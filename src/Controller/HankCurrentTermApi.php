<?php

namespace Drupal\hfc_catalog_helper\Controller;

/**
 * Defines the HANK Current Term API.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class HankCurrentTermApi extends HankTermsApi {

  /**
   * {@inheritdoc}
   */
  protected function buildQuery() {
    $query = parent::buildQuery();
    $query->condition('t.term_end_date', \Drupal::time()->getRequestTime(), '>=');
    $query->condition('t.term_reg_start_date', \Drupal::time()->getRequestTime(), '<=');
    $query->range(0, 1);

    return $query;
  }

}
