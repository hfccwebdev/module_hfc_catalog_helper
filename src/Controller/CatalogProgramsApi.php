<?php

namespace Drupal\hfc_catalog_helper\Controller;

use Drupal\node\NodeInterface;

/**
 * Defines the Catalog Programs API.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class CatalogProgramsApi extends CatalogApiBaseController {

  /**
   * {@inheritdoc}
   */
  protected function buildQuery() {
    $query = $this->database->select('node', 'n');
    $query->join('node_field_data', 'd', "n.vid = d.vid");
    $query->join('node__field_program_status', 'field_program_status', "n.vid = field_program_status.revision_id");

    $query->orderBy('d.title');
    $query->fields('n', ['nid']);

    $query->condition('d.type', 'catalog_program', '=');
    $query->condition('d.status', NodeInterface::PUBLISHED, "=");

    if (!empty($this->args['acad_level'])) {
      $query->join('node__field_acad_level', 'field_acad_level', "n.vid = field_acad_level.revision_id");
      $query->condition('field_acad_level.field_acad_level_value', $this->args['acad_level'], '=');
    }

    if (!empty($this->args['status'])) {
      $query->condition('field_program_status.field_program_status_value', $this->args['status'], '=');
    }
    else {
      $query->condition(
        'field_program_status.field_program_status_value',
        ['active1', 'active3', 'active5', 'active6'],
        'IN'
      );
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildOutput(array &$output, array $programs) {
    $entity_storage = $this->entityTypeManager->getStorage('node');
    foreach ($programs as $item) {
      $entity = $entity_storage->load($item->nid);

      if ($supp = $entity->get('field_supplemental_info')->entity) {
        $office = $supp->get('field_office_contact')->entity;
      }
      else {
        $office = NULL;
      }
      $output[$entity->id()] = [
        'nid' => $entity->id(),
        'program_code' => $entity->get('field_program_code')->value,
        'title' => $entity->label(),
        'program_name' => $entity->get('field_program_name')->value,
        'degree_type_raw' => $entity->get('field_program_type')->value,
        'degree_type' => $entity->get('field_program_type')->view()[0]['#markup'],
        'acad_level' => $entity->get('field_acad_level')->value,
        'office_id' => !empty($office) ? $office->id() : "",
        'office' => !empty($office) ? $office->label() : "",
        'path' => $entity->toUrl('canonical', ['absolute' => TRUE])->toString(),
        'program_master' => $entity->get('field_program_master')->target_id,
        'inactive' => in_array(
          $entity->get('field_program_status')->value,
          ['active2', 'inactive', 'active5', 'active6']
        ),
      ];
    }
  }

}
