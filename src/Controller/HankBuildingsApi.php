<?php

namespace Drupal\hfc_catalog_helper\Controller;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the HANK Buildings API.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class HankBuildingsApi extends CatalogApiBaseController {

  /**
   * {@inheritdoc}
   */
  protected function buildQuery() {
    $query = $this->database->select('hank_buildings', 'b');
    $query->fields('b', ['buildings_id']);
    $query->orderBy('b.buildings_id');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildOutput(&$output, $data) {
    $entity_storage = $this->entityTypeManager->getStorage('hank_building');
    foreach ($data as $item) {
      $entity = $entity_storage->load($item->buildings_id);
      $output[$entity->id()] = $this->formatOutputRow($entity);
    }
  }

  /**
   * Format output row.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to format.
   *
   * @return string[]
   *   An array of field values.
   */
  private function formatOutputRow(EntityInterface $entity) {

    return [
      'buildings_id' => $entity->id(),
      'bldg_desc' => $entity->label(),
      'bldg_location' => $entity->get('bldg_location')->value,
      'bldg_export_to_mobile' => $entity->get('bldg_export_to_mobile')->value,
    ];
  }

}
