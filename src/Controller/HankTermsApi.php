<?php

namespace Drupal\hfc_catalog_helper\Controller;

/**
 * Defines the HANK Terms API.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class HankTermsApi extends CatalogApiBaseController {

  /**
   * {@inheritdoc}
   */
  protected function buildQuery() {
    $query = $this->database->select('hank_terms', 't');
    $query->fields('t', ['terms_id']);
    $query->condition('t.terms_id', '^../..$', 'RLIKE');
    $query->orderBy('t.term_start_date');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildOutput(&$output, $data) {
    $entity_storage = $this->entityTypeManager->getStorage('hank_term');

    $format_row = function ($entity) {
      $properties = [
        'terms_id',
        'term_desc',
        'term_prereg_start_date',
        'term_reg_start_date',
        'term_start_date',
        'term_end_date',
        'term_reg_end_date',
        'term_prereg_end_date',
        'term_add_start_date',
        'term_add_end_date',
        'term_drop_end_date',
        'term_drop_start_date',
      ];
      $row = [];

      foreach ($properties as $property) {
        $row[$property] = $entity->get($property)->value;
      }
      return $row;
    };

    foreach ($data as $item) {
      $entity = $entity_storage->load($item->terms_id);
      $output[$entity->id()] = $format_row($entity);

    }
  }

}
