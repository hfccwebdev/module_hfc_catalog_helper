<?php

namespace Drupal\hfc_catalog_helper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hfc_catalog_helper\AcademicCrosswalkInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Academic Crosswalk Preview Controller.
 *
 * @package Drupal\hfc_catalog_workflow\Controller
 */
class AcademicCrosswalkPreviewController extends ControllerBase {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Stores the database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Stores the HFC Academic Crosswalk service.
   *
   * @var \Drupal\hfc_catalog_helper\AcademicCrosswalkInterface
   */
  private $crosswalk;

  /**
   * Stores the departments list.
   *
   * @var string[]
   */
  private $hankDepts;

  /**
   * Stores the divisions list.
   *
   * @var string[]
   */
  private $hankDivisions;

  /**
   * Stores the prefixes list.
   *
   * @var string[]
   */
  private $hankSubjects;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('hfc_academic_crosswalk')
    );
  }

  /**
   * Initialize the object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\hfc_catalog_helper\AcademicCrosswalkInterface $crosswalk
   *   The Course Proposal Tasks service.
   */
  public function __construct(
    Connection $database,
    AcademicCrosswalkInterface $crosswalk
  ) {
    $this->database = $database;
    $this->crosswalk = $crosswalk;

    // Load the validation array lists.
    $this->hankDepts = $this->database->query('SELECT depts_id, depts_desc FROM {hank_depts}')->fetchAllKeyed();
    $this->hankDivisions = $this->database->query('SELECT divisions_id, div_desc FROM {hank_divisions}')->fetchAllKeyed();
    $this->hankSubjects = $this->database->query('SELECT subjects_id, subj_desc FROM {hank_subjects}')->fetchAllKeyed();
  }

  /**
   * Displays the crosswalk table.
   */
  public function showCrosswalkTable() {
    $data = $this->crosswalk->getAllCrosswalkData();
    $rows = array_map(function ($row) {
      return [
        $this->t('<strong>@c</strong>', ['@c' => $row->new_school_code]),
        $this->checkPrefix($row->prefix_code),
        $this->checkDept($row->new_dept_code, $row->new_dept_name),
        $this->checkDivision($row->new_division_code, $row->new_division_name),
        $row->current_dept_code,
        $row->old_division_code,
      ];
    }, $data);

    $output['results'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('School'),
        $this->t('Prefix'),
        $this->t('New department'),
        $this->t('New division'),
        $this->t('Old department'),
        $this->t('Old division'),
      ],
      '#rows' => $rows,
      '#sticky' => TRUE,
    ];

    // Stupid Drupal.
    $output['#cache']['max-age'] = 0;

    return $output;
  }

  /**
   * Check and display prefix information.
   *
   * @param string $code
   *   The prefix to check and display.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The markup to render.
   */
  private function checkPrefix($code) {
    if (isset($this->hankSubjects[$code])) {
      return $this->formatCode($code, $this->hankSubjects[$code]);
    }
    else {
      return [
        'data' => $this->formatCode($code, 'NOT FOUND'),
        'style' => 'color: #a40f00; background: #fee',
      ];
    }
  }

  /**
   * Check and display department information.
   *
   * @param string $code
   *   The depts_id to check and display.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The markup to render.
   */
  private function checkDept($code) {
    if (isset($this->hankDepts[$code])) {
      return $this->formatCode($code, $this->hankDepts[$code]);
    }
    else {
      return [
        'data' => $this->formatCode($code, 'NOT FOUND'),
        'style' => 'color: #a40f00; background: #fee',
      ];
    }
  }

  /**
   * Check and display division information.
   *
   * @param string $code
   *   The divisions_id to check and display.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The markup to render.
   */
  private function checkDivision($code) {
    if (isset($this->hankDivisions[$code])) {
      return $this->formatCode($code, $this->hankDivisions[$code]);
    }
    else {
      return [
        'data' => $this->formatCode($code, 'NOT FOUND'),
        'style' => 'color: #a40f00; background: #fee',
      ];
    }
  }

  /**
   * Format codes and names.
   *
   * @param string $code
   *   The code value.
   * @param string $name
   *   The name value.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The markup to render.
   */
  private function formatCode($code, $name) {

    return $this->t(
      '<strong>@c</strong> - @n',
      ['@c' => $code, '@n' => $name]
    );
  }

}
