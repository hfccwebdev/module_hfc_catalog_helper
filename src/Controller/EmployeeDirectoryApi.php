<?php

namespace Drupal\hfc_catalog_helper\Controller;

use Drupal\Core\Database\Query\Condition;

/**
 * Defines the Employee Directory API.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class EmployeeDirectoryApi extends CatalogApiBaseController {

  /**
   * {@inheritdoc}
   */
  protected function buildQuery() {
    $query = $this->database->select('hank_hrper', 'h');

    $query->leftJoin('staffdir__field_opt_out', 'field_opt_out', "h.hrper_id = field_opt_out.entity_id");

    $query->fields('h', ['hrper_id']);

    if (empty($this->args['all'])) {
      $query->condition(
        'h.pospay_bargaining_unit',
        ['XADM', 'XSEC', 'LO71', '1650', 'SSA', 'DSOE', 'ADJT'],
        'IN'
      );
    }

    $opt_out = (new Condition('OR'))
      ->isNull('field_opt_out.field_opt_out_value')
      ->condition('field_opt_out.field_opt_out_value', 0);

    $query->condition($opt_out);

    if (!empty($this->args['id'])) {
      $query->condition('h.hrper_id', $this->args['id'], '=');
    }

    if (!empty($this->args['department'])) {
      $query->condition('h.pos_dept', $this->args['department'], '=');
    }

    if (!empty($this->args['division'])) {
      $query->condition('h.pos_division_idx', $this->args['division'], '=');
    }

    if (!empty($this->args['firstname'])) {
      $query->condition('h.first_name', $this->args['firstname'], '=');
    }

    if (!empty($this->args['lastname'])) {
      $query->condition('h.last_name', $this->args['lastname'], '=');
    }

    if (!empty($this->args['username'])) {
      $query->condition('h.h19_idv_oee_username', $this->args['username'], '=');
    }

    $sort = $this->args['sort'] ?? NULL;
    switch ($sort) {
      case 'id':
        $query->orderBy('hrper_id');
        break;

      case 'fn':
      case 'first_name';
        $query->orderBy('first_name');
        $query->orderBy('last_name');
        $query->orderBy('hrper_id');
        break;

      case 'email':
      case 'username':
        $query->orderBy('h19_idv_oee_username');
        break;

      default:
        $query->orderBy('last_name');
        $query->orderBy('first_name');
        $query->orderBy('hrper_id');
        break;
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildOutput(array &$output, array $employees) {
    $entity_storage = $this->entityTypeManager->getStorage('staffdir');
    foreach ($employees as $item) {
      $entity = $entity_storage->load($item->hrper_id);
      $output[$entity->id()] = [
        'hank_id' => $entity->id(),
        'username' => $entity->get('h19_idv_oee_username')->value,
        'lastname' => $entity->get('last_name')->value,
        'firstname' => $entity->get('first_name')->value,
        'title' => $entity->get('pos_title')->value,
        'division' => $entity->get('pos_division_idx')->target_id,
        'department' => $entity->get('pos_dept')->target_id,
        'barg_unit' => $entity->get('pospay_bargaining_unit')->value,
        'building' => $entity->get('campus_building')->value,
        'office_location' => $entity->get('campus_office')->value,
        'email_address' => $entity->get('person_email_addresses')->value,
        'phone_number' => $entity->officePhone(),
        'contact' => $entity->contactInfo(),
        'credentials' => $entity->credentials(),
        'suffix' => $entity->suffix(),
      ];
    }
  }

}
