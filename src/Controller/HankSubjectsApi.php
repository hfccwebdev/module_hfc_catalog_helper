<?php

namespace Drupal\hfc_catalog_helper\Controller;

/**
 * Defines the Hank Subjects API.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class HankSubjectsApi extends CatalogApiBaseController {

  /**
   * {@inheritdoc}
   */
  protected function buildQuery() {
    $query = $this->database->select('hank_subjects', 's');

    $query->join('node__field_crs_subject', 'field_crs_subject', "s.subjects_id = field_crs_subject.field_crs_subject_target_id");
    $query->join('node', 'n', "n.vid = field_crs_subject.revision_id");
    $query->join('node__field_school', 'field_school', "field_school.revision_id = n.vid");

    $query->fields('s', ['subjects_id', 'subj_desc']);

    $query->condition('s.subj_is_active', '1');
    $query->condition('n.type', 'catalog_course');

    if (!empty($this->args['school'])) {
      $query->condition('field_school.field_school_value', $this->args['school']);
    }

    $query->orderBy('subjects_id');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildOutput(&$output, $subjects) {
    foreach ($subjects as $item) {
      $output[$item->subjects_id] = $item;
    }
  }

}
