<?php

namespace Drupal\hfc_catalog_helper\Controller;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the Course Sections API contents.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class CourseSectionsApi extends CatalogApiBaseController {

  /**
   * {@inheritdoc}
   */
  protected function buildQuery() {
    $query = $this->database->select('hank_course_sections', 's');
    $query->fields('s', ['course_sections_id']);
    $query->join('hank_courses', 'c', "s.sec_course = c.courses_id");
    $query->join('hank_terms', 't', "s.sec_term = t.terms_id");

    if (!empty($this->args['sec_term'])) {
      $query->condition('s.sec_term', $this->args['sec_term'], '=');
    }

    if (!empty($this->args['sec_subject'])) {
      $query->condition('s.sec_subject', $this->args['sec_subject'], '=');
    }

    if (!empty($this->args['sec_course_no'])) {
      $query->condition('s.sec_course_no', $this->args['sec_course_no'], '=');
    }

    if (!empty($this->args['crs_name'])) {
      $query->condition('c.crs_name', $this->args['crs_name'], '=');
    }

    if (!empty($this->args['sec_no'])) {
      $query->condition('s.sec_no', $this->args['sec_no'], '=');
    }

    if (!empty($this->args['sec_status'])) {
      $query->condition('s.sec_status', $this->args['sec_status'], '=');
    }

    if (!empty($this->args['sec_sched_type'])) {
      $query->condition('s.sec_sched_type', $this->args['sec_sched_type'], '=');
    }

    $query->orderBy('t.term_start_date');
    $query->orderBy('c.crs_name');
    $query->orderBy('s.sec_no');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildOutput(&$output, $data) {
    $entity_storage = $this->entityTypeManager->getStorage('hank_course_section');

    foreach ($data as $item) {
      $entity = $entity_storage->load($item->course_sections_id);
      $output[$entity->id()] = $this->formatOutputRow($entity);
    }
  }

  /**
   * Format output row.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to format.
   *
   * @return string[]
   *   An array of field values.
   */
  private function formatOutputRow(EntityInterface $entity) {

    $sec_instr_method = $this->catalogHelper->getInstrMethods()[$entity->get('sec_instr_methods')->value]
      ?? $entity->get('sec_instr_methods')->value;

    $faculty = $entity->get('sec_faculty')->entity ?? NULL;

    return [
      'course_sections_id' => $entity->id(),
      'sec_term' => $entity->get('sec_term')->target_id,
      'crs_name' => $entity->get('sec_course')->entity->label(),
      'sec_subject' => $entity->get('sec_subject')->value,
      'sec_course_no' => $entity->get('sec_course_no')->value,
      'sec_no' => $entity->get('sec_no')->value,
      'sec_short_title' => $entity->get('sec_short_title')->value,
      'crs_contact_hours' => number_format($entity->get('sec_course')->entity->get('crs_contact_hours')->value, 2),
      'crs_min_cred' => number_format($entity->get('sec_course')->entity->get('crs_min_cred')->value, 2),
      'sec_instr_methods' => $sec_instr_method,
      'faculty_first_name' => !empty($faculty) ? $faculty->get('first_name')->value : NULL,
      'faculty_last_name' => !empty($faculty) ? $faculty->get('last_name')->value : NULL,
      'faculty_email' => !empty($faculty) ? $faculty->get('person_email_addresses')->value : NULL,
      'sec_sched_type' => $entity->get('sec_sched_type')->value,
      'sec_meeting_days' => preg_replace('/_/', '', $entity->get('sec_meeting_days')->value),
      'sec_meeting_start_time' => $entity->get('sec_meeting_start_time')->value,
      'sec_meeting_end_time' => $entity->get('sec_meeting_end_time')->value,
      'sec_is_online' => $entity->get('sec_is_online')->value,
      'sec_bldg' => $entity->get('sec_bldg')->target_id,
      'sec_room' => $entity->get('sec_room')->value,
      'sec_capacity' => $entity->get('sec_capacity')->value,
      'sec_seats_taken' => $entity->get('sec_seats_taken')->value,
      'sec_waitlist_count' => $entity->get('sec_waitlist_count')->value,
      'sec_status' => $entity->get('sec_status')->value,
      'sec_status_date' => $this->dateFormatter->format($entity->get('sec_status_date')->value, 'custom', 'm/d/Y'),
    ];
  }

}
