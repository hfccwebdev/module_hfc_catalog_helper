<?php

namespace Drupal\hfc_catalog_helper\Controller;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the HANK Divisions API.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class HankDivisionsApi extends CatalogApiBaseController {

  /**
   * {@inheritdoc}
   */
  protected function buildQuery() {
    $query = $this->database->select('hank_divisions', 'd');
    $query->fields('d', ['divisions_id']);
    $query->orderBy('d.divisions_id');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildOutput(&$output, $data) {
    $entity_storage = $this->entityTypeManager->getStorage('hank_division');
    foreach ($data as $item) {
      $entity = $entity_storage->load($item->divisions_id);
      $output[$entity->id()] = $this->formatOutputRow($entity);
    }
  }

  /**
   * Format output row.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to format.
   *
   * @return string[]
   *   An array of field values.
   */
  private function formatOutputRow(EntityInterface $entity) {

    $office = $entity->get('field_office')->entity ?? NULL;

    return [
      'divisions_id' => $entity->id(),
      'div_desc' => $entity->label(),
      'div_school' => $entity->get('div_school')->value,
      'url' => !empty($office) ? $office->field_website->uri : NULL,
      'div_head_id' => $entity->get('div_head_id')->value,
      'office_id' => !empty($office) ? $office->id() : NULL,
    ];
  }

}
