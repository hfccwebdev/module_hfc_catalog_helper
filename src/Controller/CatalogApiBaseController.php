<?php

namespace Drupal\hfc_catalog_helper\Controller;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines the Catalog REST API base structure.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
abstract class CatalogApiBaseController implements ContainerInjectionInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Stores the query arguments.
   *
   * @var array
   */
  protected $args;

  /**
   * Drupal\Core\Datetime\DateFormatter definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Drupal\hfc_catalog_helper\CatalogUtilitiesInterface definition.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $catalogHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('request_stack'),
      $container->get('date.formatter'),
      $container->get('hfc_catalog_helper')
    );
  }

  /**
   * Constructs a new Controller object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The active database connection.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The Symfony request stack.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The Date Formatter service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $catalog_helper
   *   The HFC Catalog Helper tools.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    Connection $database,
    RequestStack $request_stack,
    DateFormatter $date_formatter,
    CatalogUtilitiesInterface $catalog_helper
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->args = $request_stack->getCurrentRequest()->query->all();
    $this->dateFormatter = $date_formatter;
    $this->catalogHelper = $catalog_helper;
  }

  /**
   * Display the endpoint contents.
   */
  public function view() {

    $output = [];

    $query = $this->buildQuery();

    if ($results = $query->execute()->fetchAll()) {
      $this->buildOutput($output, $results);
    }

    $response = new JsonResponse($output);
    return $response;
  }

  /**
   * Builds the database query.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   The database select query.
   */
  abstract protected function buildQuery();

  /**
   * Builds the renderable output.
   *
   * @param array $output
   *   The renderable output array.
   * @param array $data
   *   The data to render.
   */
  abstract protected function buildOutput(array &$output, array $data);

}
