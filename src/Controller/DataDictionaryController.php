<?php

namespace Drupal\hfc_catalog_helper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds data dictionary reports for Courses and Programs.
 *
 * @package Drupal\hfc_catalog_helper\Controller
 */
class DataDictionaryController extends ControllerBase {

  /**
   * Stores the Entity Field Manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The Entity Field Manager service.
   */
  public function __construct(EntityFieldManagerInterface $entityFieldManager) {
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * View course family dictionary.
   */
  public function viewCourseDictionary() {
    $output = [];
    $content_types = [
      'course_proposal',
      'course_master',
      'supplemental_course_info',
      'catalog_course',
    ];
    $output['results'] = $this->getDictionary($content_types);
    $output['#attached']['library'][] = 'hfc_catalog_helper/data_dictionary';
    return $output;
  }

  /**
   * View program family dictionary.
   */
  public function viewProgramDictionary() {
    $output = [];
    $content_types = [
      'program_proposal',
      'program_master',
      'supplemental_program_info',
      'catalog_program',
    ];
    $output['results'] = $this->getDictionary($content_types);
    $output['#attached']['library'][] = 'hfc_catalog_helper/data_dictionary';
    return $output;
  }

  /**
   * Load data dictionary for an array of content types.
   */
  private function getDictionary(array $content_types): array {

    $dictionary = [];
    $rows = [];

    foreach ($content_types as $bundle) {
      $bundle_fields = $this->entityFieldManager->getFieldDefinitions('node', $bundle);
      foreach ($bundle_fields as $field_name => $field_type) {
        if (preg_match('/^field_/', $field_name)) {
          $dictionary[$field_name][$bundle] = $field_type;
        }
      }
    }

    ksort($dictionary);

    foreach ($dictionary as $field_name => $field) {
      foreach ($content_types as $bundle) {
        if (isset($field[$bundle])) {
          $row[$bundle] = [
            'data' => $field_name,
            'class' => [
              $field[$bundle]->isRequired() ? 'required' : 'optional',
            ],
          ];
        }
        else {
          $row[$bundle] = [
            'data' => NULL,
            'class' => 'unused',
          ];
        }
      }
      $rows[$field_name] = $row;
    }

    return [
      '#type' => 'table',
      '#header' => $content_types,
      '#rows' => $rows,
      '#sticky' => TRUE,
      '#caption' => $this->t('Data dictionary for courses.'),
      '#attributes' => ['class' => ['data-dictionary-report']],
      '#empty' => $this->t('Could not build dictionary for specified content types.'),
    ];
  }

}
