<?php

namespace Drupal\hfc_catalog_helper;

/**
 * Defines the HFC Academic Crosswalk Interface.
 *
 * @package Drupal\hfc_catalog_helper
 */
interface AcademicCrosswalkInterface {

  /**
   * Retrieve all crosswalk data.
   *
   * @return \StdClass[]
   *   An array of crosswalk data records.
   */
  public function getAllCrosswalkData();

  /**
   * Get all new departments and schools.
   *
   * @return \StdClass[]
   *   An array of department and school codes.
   */
  public function getAllDepts();

  /**
   * Get new school code from new department.
   *
   * @param string $dept
   *   The current department code.
   *
   * @return string
   *   The new school code for the department.
   */
  public function getSchoolByDept($dept);

  /**
   * Get new department code from subject.
   *
   * @param string $prefix
   *   The current subject prefix.
   *
   * @return string
   *   The new department code for the subject.
   */
  public function getDeptBySubject($prefix);

  /**
   * Get subject prefix list from new school code.
   *
   * @param string $school
   *   The school code to query.
   *
   * @return array
   *   The subject prefix list.
   */
  public function getSubjectsBySchool($school);

  /**
   * Get all subject to department mappings.
   *
   * @return array
   *   A list of departments, keyed by subject.
   */
  public function getSubjectDepts();

  /**
   * Get missing subject to department mappings.
   *
   * @return array
   *   An array of prefix_code with missing new_dept_code mappings.
   */
  public function getSubjectMissingDept();

  /**
   * Gets a course crosswalk exception record.
   *
   * @param string $prefix
   *   The subject prefix to check.
   * @param string $number
   *   The course number to check.
   *
   * @return object|null
   *   Matching exception info or null if not found.
   */
  public function getCourseCrosswalkException($prefix, $number);

}
