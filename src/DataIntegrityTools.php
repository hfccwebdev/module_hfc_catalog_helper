<?php

namespace Drupal\hfc_catalog_helper;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\hfc_hank_api\HfcHankApiInterface;
use Drupal\node\Entity\Node;

/**
 * Defines the Data Integrity Tools service.
 *
 * @package Drupal\hfc_catalog_helper
 */
class DataIntegrityTools implements DataIntegrityToolsInterface {

  /**
   * Stores the database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Stores the HANK API service.
   *
   * @var \Drupal\hfc_hank_api\HfcHankApiInterface
   */
  protected $hankApi;

  /**
   * Stores the Catalog Helper service.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\hfc_hank_api\HfcHankApiInterface $hankApi
   *   The HANK API service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $hfc_catalog_helper
   *   The Catalog Helper service.
   */
  public function __construct(
    Connection $connection,
    HfcHankApiInterface $hankApi,
    CatalogUtilitiesInterface $hfc_catalog_helper
  ) {
    $this->connection = $connection;
    $this->hankApi = $hankApi;
    $this->helper = $hfc_catalog_helper;
  }

  /**
   * {@inheritdoc}
   */
  public function staffdirMissingRefReport() {
    $output = ["Checking for missing Staff Directory References on Catalog Website."];
    $missing = $this->staffdirMissingReferences();

    if (!empty($missing['office'])) {
      $output[] = PHP_EOL . "Missing Office Administrator references" . PHP_EOL;
      foreach ($missing['office'] as $item) {
        $output[] = "\"" . implode('","', (array) $item) . "\"";
      }
    }
    else {
      $output[] = PHP_EOL . "No missing Office Administrator references.";
    }

    if (!empty($missing['program'])) {
      $output[] = PHP_EOL . "Missing Program Contacts references" . PHP_EOL;
      foreach ($missing['program'] as $item) {
        $output[] = "\"" . implode('","', (array) $item) . "\"";
      }
    }
    else {
      $output[] = PHP_EOL . "No missing Program Contacts references.";
    }

    return PHP_EOL . implode(PHP_EOL, $output) . PHP_EOL;
  }

  /**
   * {@inheritdoc}
   */
  public function staffdirMissingReferences() {

    $output = [];

    $query = $this->connection->select('node', 'n');
    $query->join('node_field_data', 'nd', "n.nid = nd.nid AND n.vid = nd.vid");
    $query->leftJoin('node__field_administrator', 'f', "n.nid = f.entity_id AND n.vid = f.revision_id");
    $query->leftJoin('hank_hrper', 'h', "f.field_administrator_target_id = h.hrper_id");

    $query->fields('nd', ['nid', 'title']);
    $query->addField('f', 'field_administrator_target_id', 'tid');

    $query->condition('n.type', 'office');
    $query->condition('nd.status', Node::PUBLISHED);
    $query->isNotNull('f.field_administrator_target_id');
    $query->isNull('h.last_name');

    $query->orderBy('nd.title');

    $output['office'] = $query->execute()->fetchAll();

    $query = $this->connection->select('node', 'n');
    $query->join('node_field_data', 'nd', "n.nid = nd.nid AND n.vid = nd.vid");
    $query->leftJoin('node__field_faculty_contact', 'f', "n.nid = f.entity_id AND n.vid = f.revision_id");
    $query->leftJoin('hank_hrper', 'h', "f.field_faculty_contact_target_id = h.hrper_id");
    $query->join('node__field_program_master', 'pp', "n.nid = pp.entity_id AND n.vid = pp.revision_id");
    $query->join('node', 'pm', "pp.field_program_master_target_id = pm.nid");
    $query->join('node__field_program_status', 's', "pm.nid = s.entity_id and pm.vid = s.revision_id");

    $query->fields('nd', ['nid', 'title']);
    $query->addField('f', 'field_faculty_contact_target_id', 'tid');

    $query->condition('n.type', 'supplemental_program_info');
    $query->condition('s.field_program_status_value', ['active1', 'active3'], 'IN');
    $query->isNotNull('f.field_faculty_contact_target_id');
    $query->isNull('h.last_name');

    $query->orderBy('nd.title');

    $output['program'] = $query->execute()->fetchAll();

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function staffdirMissingCredentialsReport() {

    $output = ["Checking for missing Staff Directory Credentials on Catalog Website."];
    $missing = $this->staffdirMissingCredentials();

    if (!empty($missing)) {
      $other_degrees = $this->getHankOtherDegrees();
      $output[] = PHP_EOL . "Missing Degree types" . PHP_EOL;
      foreach ($missing as $item) {
        $output[] = "* " . $item->hrper_degree . ": " . (!empty($other_degrees[$item->hrper_degree]) ? $other_degrees[$item->hrper_degree] : 'No match found in HANK API');
      }
    }
    else {
      $output[] = PHP_EOL . "No missing Degree Types.";
    }

    return PHP_EOL . implode(PHP_EOL, $output) . PHP_EOL;
  }

  /**
   * {@inheritdoc}
   */
  public function checkLegacyContactHours() {
    $conditions = [
      ['field' => 'field_inactive', 'value' => '1', 'operator' => '<>'],
    ];

    $nids = $this->helper->getNidsByType('course_master', $conditions);

    echo "nid,course,legacy_instructor_hours,current_instructor_hours,legacy_student_hours,current_student_hours" . PHP_EOL;

    foreach ($nids as $nid) {
      $master = Node::load($nid);

      if ($legacy_hours = $this->getLegacyContactHours($master)) {

        $current_instructor_hours = $master->field_crs_instructor_hours->value;
        $current_student_hours = $master->field_crs_student_hours->value;

        $legacy_instructor_hours = $legacy_hours->field_crs_instructor_hours_value;
        $legacy_student_hours = $legacy_hours->field_crs_student_hours_value;

        if (
          ($current_instructor_hours !== $legacy_instructor_hours) ||
          ($current_student_hours !== $legacy_student_hours)
        ) {
          $nid = $master->id();
          $subject = $master->field_crs_subject->target_id;
          $number = $master->field_crs_number->value;
          echo "$nid,$subject-$number,$legacy_instructor_hours,$current_instructor_hours,$legacy_student_hours,$current_student_hours" . PHP_EOL;
        }
      }
    }
  }

  /**
   * Query HANK credentials tables for missing data.
   */
  private function staffdirMissingCredentials() {
    $query = "SELECT hrper_degree FROM hank_hrper_credentials WHERE hrper_degree NOT IN (SELECT degree_type FROM staffdir_degree_types) GROUP BY hrper_degree ORDER BY hrper_degree";
    $result = $this->connection->query($query)->fetchAll();

    return $result;
  }

  /**
   * Query the HANK Other Degrees table using the API.
   */
  private function getHankOtherDegrees() {

    $items = $this->hankApi->getData("other-degrees", NULL);
    $output = [];
    foreach ($items as $item) {
      $output[$item->OTHER_DEGREES_ID] = $item->ODEG_DESC . ' (' . $item->ODEG_TYPE . ') ' . $item->OTHER_DEGREES_ADDOPR . ' ' . $item->OTHER_DEGREES_ADDDATE;
    }
    return $output;
  }

  /**
   * Get Contact Hours from Legacy Courses website.
   */
  private function getLegacyContactHours(Node $master) {
    $subject = $master->field_crs_subject->target_id;
    $number = $master->field_crs_number->value;

    // Switch database connection to courseslegacy website.
    Database::setActiveConnection('courseslegacy');

    $database = Database::getConnection();
    $query = $database->select('node', 'n');

    $query->join('field_data_field_crs_subj', 'subj', "n.vid = subj.revision_id AND subj.entity_type = 'node'");
    $query->join('field_data_field_crs_num', 'num', "n.vid = num.revision_id AND num.entity_type = 'node'");
    $query->join('field_data_field_crs_instructor_hours', 'ins', "n.vid = ins.revision_id AND ins.entity_type = 'node'");
    $query->join('field_data_field_crs_student_hours', 'stu', "n.vid = stu.revision_id AND stu.entity_type = 'node'");

    $query->condition('type', 'course_master');
    $query->condition('subj.field_crs_subj_value', $subject);
    $query->condition('num.field_crs_num_value', $number);

    $query->fields('n', ['nid', 'title']);
    $query->fields('ins', ['field_crs_instructor_hours_value']);
    $query->fields('stu', ['field_crs_student_hours_value']);

    $output = $query->execute()->fetchAll();

    // Switch database connection back to default!
    Database::setActiveConnection();

    if ($output) {
      return reset($output);
    }
  }

}
