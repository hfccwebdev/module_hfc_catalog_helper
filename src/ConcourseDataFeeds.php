<?php

namespace Drupal\hfc_catalog_helper;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;
use Drupal\hankdata\HankDataInterface;
use Drupal\hfc_hank_api\HfcHankApiInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;

/**
 * Defines the Catalog Archive Builder Service.
 *
 * @package Drupal\hfc_catalog_helper
 */
class ConcourseDataFeeds implements ConcourseDataFeedsInterface {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Configuration settings name.
   *
   * @var string
   */
  const SETTINGS = 'hfc_catalog_helper.concourse_data_settings';

  /**
   * Define header strings for feed.
   */
  const HEADER_ROW = [
    'Campus' => 'CAMPUS_IDENTIFIER|NAME',
    'School' => 'SCHOOL_IDENTIFIER|NAME',
    'Department' => 'SCHOOL_IDENTIFIER|DEPARTMENT_IDENTIFIER|NAME',
    'Course' => 'COURSE_IDENTIFIER|TITLE|CAMPUS_IDENTIFIER|DEPARTMENT_IDENTIFIER|START_DATE|END_DATE|CLONE_FROM_IDENTIFIER|TIMEZONE|PREFIX|NUMBER|INSTRUCTOR|SESSION|YEAR|CREDITS|DELIVERY_METHOD|IS_STRUCTURED|IS_TEMPLATE|HIDDEN_FROM_SEARCH',
    'Section' => 'COURSE_IDENTIFIER|SECTION_IDENTIFIER|SECTION_LABEL',
    'Description' => 'COURSE_IDENTIFIER|DESCRIPTION|REQUISITES|NOTES|COMMENTS|IS_LOCKED',
    'Objective' => 'COURSE_IDENTIFIER|OBJECTIVES|NOTES|COMMENTS|IS_LOCKED',
    'Outcome' => 'COURSE_IDENTIFIER|OUTCOMES|NOTES|COMMENTS|IS_LOCKED',
  ];

  /**
   * Stores the Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Stores the Database Connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * Stores the Date Formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  private $dateFormatter;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Stores the Entity Field Manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private $entityFieldManager;

  /**
   * Stores the Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Stores the GuzzleHttp Client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * Stores the Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * Stores the HANK Data Service.
   *
   * @var \Drupal\hankdata\HankDataInterface
   */
  private $hankData;

  /**
   * Stores the HFC Academic Crosswalk service.
   *
   * @var \Drupal\hfc_catalog_helper\AcademicCrosswalkInterface|null
   */
  private $academicCrosswalk;

  /**
   * Stores the HFC HANK API client service.
   *
   * @var \Drupal\hfc_hank_api\HfcHankApiInterface
   */
  protected $hankApi;

  /**
   * Stores a copy of the relevant HANK Term data.
   *
   * @var \Drupal\hankdata\HankTermInterface
   */
  private $currentTerm;

  /**
   * The default site timezone.
   *
   * @var string
   */
  private $timezone;

  /**
   * An array to store reusable course master info.
   *
   * @var array
   */
  private static $masters = [];

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The Date Formatter service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP Client.
   * @param \Drupal\hankdata\HankDataInterface $hankdata
   *   The HANK Data service.
   * @param \Drupal\hfc_catalog_helper\AcademicCrosswalkInterface $academicCrosswalk
   *   The Academic Crosswalk service.
   * @param \Drupal\hfc_hank_api\HfcHankApiInterface $hfc_hank_api
   *   The HANK API service.
   */
  public function __construct(
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    RendererInterface $renderer,
    ConfigFactoryInterface $config_factory,
    DateFormatterInterface $date_formatter,
    TimeInterface $time,
    ClientInterface $http_client,
    HankDataInterface $hankdata,
    AcademicCrosswalkInterface $academicCrosswalk,
    HfcHankApiInterface $hfc_hank_api
  ) {
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->renderer = $renderer;
    $this->configFactory = $config_factory;
    $this->dateFormatter = $date_formatter;
    $this->time = $time;
    $this->httpClient = $http_client;
    $this->hankData = $hankdata;
    $this->academicCrosswalk = $academicCrosswalk;
    $this->hankApi = $hfc_hank_api;

    // Timezone is needed for Course feeds.
    $this->timezone = $this->configFactory->get("system.date")->get("timezone")['default'];
  }

  /**
   * {@inheritdoc}
   */
  public function extract($feedName, array $options = []): ?string {

    if ($feedName == 'Template') {
      return (
        $this->extract('CourseTemplate', $options) . PHP_EOL .
        $this->extract('SectionTemplate', $options) . PHP_EOL .
        $this->extract('Description', $options) . PHP_EOL .
        $this->extract('Objective', $options) . PHP_EOL .
        $this->extract('Outcome', $options)
      );
    }
    elseif ($feedName == 'Clone') {
      return (
        $this->extract('Course', $options) . PHP_EOL .
        $this->extract('Section', $options)
      );
    }

    $extractMethod = 'extract' . $feedName;
    if (method_exists(__CLASS__, $extractMethod)) {

      // Load academic term data. If no term was specified,
      // load the current term and add it to $options.
      if (!isset($options['term'])) {
        $options['term'] = $this->hankData->getHankCurrentTerm();
      }

      $this->currentTerm = $this->entityTypeManager->getStorage('hank_term')->load($options['term']);

      if (empty($this->currentTerm)) {
        $this->messenger()->addError(
          $this->t(
            'Could not load requested term @id.',
            ['@id' => $options['term']]
          )
        );
        return NULL;
      }

      $timestamp = $this->time->getRequestTime();

      // Trim off fake "Template" part of feed names for headers.
      $headerName = preg_replace(
        ['/DeptTemplate$/', '/Template$/'],
        ['', ''],
        $feedName
      );

      // Perform the requested extract.
      return implode(PHP_EOL, [
        self::HEADER_ROW[$headerName],
        '',
        "# $feedName feed generated " . $this->dateFormatter->format($timestamp, 'custom', 'm/d/Y g:ia'),
        '',
        $this->$extractMethod($options),
        '',
        '### End of feed file ###',
      ]) . PHP_EOL;
    }
    else {
      $this->messenger()->addError(
        $this->t(
          'Concourse Feed @name is not defined.',
          ['@name' => $feedName]
        ));
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function upload($feedName, array $options = []): bool {

    if ($feedName == 'Template') {
      return (
        $this->upload('CourseTemplate', $options) &&
        $this->upload('SectionTemplate', $options)  &&
        $this->upload('Description', $options)  &&
        $this->upload('Objective', $options) &&
        $this->upload('Outcome', $options)
      );
    }
    elseif ($feedName == 'Clone') {
      return (
        $this->upload('Course', $options) &&
        $this->upload('Section', $options)
      );
    }

    $contents = $this->extract($feedName, $options);

    if (empty($contents)) {
      $this->messenger()->addError($this->t('Cannot upload empty extract.'));
      return FALSE;
    }

    // Trim off fake "Template" part of feed names.
    $feedName = preg_replace(
      ['/DeptTemplate$/', '/Template$/'],
      ['', ''],
      $feedName
    );

    $settings = $this->configFactory->get(static::SETTINGS);

    $timestamp = $this->dateFormatter->format(
      $this->time->getRequestTime(),
      'custom',
      'Ymd_His'
    );

    $filename = "concourse_" . mb_strtolower($feedName) . "_{$timestamp}.txt";
    $hash = hash_hmac('sha256', $contents, $settings->get('shared_secret'));

    $response = $this->httpClient->request('POST', $settings->get('destination_url'), [
      RequestOptions::MULTIPART => [
        ['name' => 'type', 'contents' => $feedName],
        ['name' => 'hmac', 'contents' => $hash],
        ['name' => 'file', 'contents' => $contents, 'filename' => $filename],
      ],
      RequestOptions::TIMEOUT => 300,
    ]);
    if ($response->getStatusCode() == 200) {
      return TRUE;
    }
    else {
      $this->messenger()->addWarning($this->t(
        'Concourse upload returned response @status: @message.',
        [
          '@status' => $response->getStatusCode(),
          '@message' => $response->getReasonPhrase(),
        ]
      ));
    }
  }

  /**
   * Extract Campus feed.
   */
  private function extractCampus(array $options): string {
    return $this->configFactory->get(static::SETTINGS)->get('campus_feed') . PHP_EOL;
  }

  /**
   * Extract School feed.
   */
  private function extractSchool(array $options): string {
    return $this->configFactory->get(static::SETTINGS)->get('school_feed') . PHP_EOL;
  }

  /**
   * Extract Department feed.
   */
  private function extractDepartment(array $options): string {
    $crosswalk = $this->academicCrosswalk->getAllDepts();
    $schools = array_keys($this->getSchoolOpts());
    $output = array_map(function ($dept) use ($schools) {
      $school = in_array($dept->school, $schools) ? $dept->school : 'Other';
      return $school . "|" . $dept->dept . "|" . $dept->name;
    }, $crosswalk);
    return implode(PHP_EOL, $output) . PHP_EOL;
  }

  /**
   * Extract Course Department Template feed.
   */
  private function extractCourseDeptTemplate(array $options): string {
    $crosswalk = $this->academicCrosswalk->getAllDepts();
    $rows = array_map('self::getCourseDeptTemplateRow', $crosswalk);
    return implode(PHP_EOL, $rows) . PHP_EOL;
  }

  /**
   * Extract Course Template feed.
   *
   * This outputs the same format as the Course feed,
   * but will be used as a template for the actual Course feed,
   * which is based on HANK Course Sections.
   */
  private function extractCourseTemplate(array $options): string {
    $nids = $this->getCurrentCourseNids($options);
    $rows = array_map('self::getCourseTemplateRow', $nids);
    return implode(PHP_EOL, $rows) . PHP_EOL;
  }

  /**
   * Extract Course feed.
   *
   * This extracts the actual course syllabus records,
   * keyed by course_sections_id to match up with course information
   * coming to Concourse from the LMS. These records specify a
   * CLONE_FROM_IDENTIFIER corresponding to the templates
   * created by the extractCourseTemplate method.
   */
  private function extractCourse(array $options): string {
    $ids = $this->getCourseSectionsIds($options);
    $rows = [];
    foreach ($ids as $id) {
      $row = $this->getCourseRow($id);
      if (!empty($row)) {
        $rows[] = $row;
      }
    }
    return implode(PHP_EOL, $rows) . PHP_EOL;
  }

  /**
   * Extract Section Department Section feed.
   */
  private function extractSectionDeptTemplate(array $options): string {
    $crosswalk = $this->academicCrosswalk->getAllDepts();
    $output = array_map(function ($dept) {
      return "{$dept->dept}_DEPT_TMPL|{$dept->dept}_DEPT_TMPL_SEC|Template";
    }, $crosswalk);
    return implode(PHP_EOL, $output) . PHP_EOL;
  }

  /**
   * Extract Section Template feed.
   *
   * This generates a Section feed for courses in the
   * CourseTemplate feed. It is used to add the word
   * "Template" in the course listing on Concourse.
   */
  private function extractSectionTemplate(array $options): string {
    $nids = $this->getCurrentCourseNids($options);
    $rows = array_map('self::getSectionTemplateRow', $nids);
    return implode(PHP_EOL, $rows) . PHP_EOL;
  }

  /**
   * Extract Section feed.
   *
   * This generates a Section feed for individual syllabi
   * in the Course feed.
   */
  private function extractSection(array $options): string {
    $ids = $this->getCourseSectionsIds($options);
    $rows = [];
    foreach ($ids as $id) {
      $row = $this->getSectionRow($id);
      if (!empty($row)) {
        $rows[] = $row;
      }
    }
    return implode(PHP_EOL, $rows) . PHP_EOL;
  }

  /**
   * Extract Description feed.
   *
   * Extract Catalog Course descriptions and add to
   * Concourse templates already created by CourseTemplate
   * feed.
   */
  private function extractDescription(array $options): string {
    $nids = $this->getCurrentCourseNids($options);
    $rows = array_map('self::getDescriptionRow', $nids);
    return implode(PHP_EOL, $rows) . PHP_EOL;
  }

  /**
   * Extract Objective feed.
   *
   * Extract Catalog Course objectives and add to
   * Concourse templates already created by CourseTemplate
   * feed.
   */
  private function extractObjective(array $options): string {
    $nids = $this->getCurrentCourseNids($options);
    $rows = array_map('self::getObjectiveRow', $nids);
    return implode(PHP_EOL, $rows) . PHP_EOL;
  }

  /**
   * Extract Outcome feed.
   *
   * Extract Catalog Course outcomes and add to
   * Concourse templates already created by CourseTemplate
   * feed.
   */
  private function extractOutcome(array $options): string {
    $nids = $this->getCurrentCourseNids($options);
    $rows = array_map('self::getOutcomeRow', $nids);
    return implode(PHP_EOL, $rows) . PHP_EOL;
  }

  /**
   * Get a single row for the course department template extract.
   *
   * @param object $dept
   *   The department object from AcademicCrosswalk to use as the source.
   *
   * @return string
   *   A string formatted as a Concourse Course feed record.
   *
   * @todo This will need a complete rewrite once we can use actual
   * department data.
   */
  private function getCourseDeptTemplateRow($dept): string {

    $output = [
      'COURSE_IDENTIFIER' => "{$dept->dept}_DEPT_TMPL",
      'TITLE' => "{$dept->name} Department Template",
      'CAMPUS_IDENTIFIER' => 'TMPL',
      'DEPARTMENT_IDENTIFIER' => $dept->dept,
      'START_DATE' => '01/01/2021',
      'END_DATE' => '01/01/2099',
      'CLONE_FROM_IDENTIFIER' => "{$dept->school}_TMPL",
      'TIMEZONE' => $this->timezone,
      'PREFIX' => '',
      'NUMBER' => '',
      'INSTRUCTOR' => '',
      'SESSION' => '',
      'YEAR' => '',
      'CREDITS' => '',
      'DELIVERY_METHOD' => '',
      'IS_STRUCTURED' => 1,
      'IS_TEMPLATE' => 1,
      'HIDDEN_FROM_SEARCH' => 0,
    ];
    return implode('|', $output);
  }

  /**
   * Get a single row for the course template extract.
   *
   * @param int $nid
   *   The node ID of the Catalog Course to use as the source.
   *
   * @return string
   *   A string formatted as a Concourse Course feed record.
   */
  private function getCourseTemplateRow($nid): string {

    if ($node = $this->entityTypeManager->getStorage('node')->load($nid)) {

      $prefix = $node->field_crs_subject->target_id;
      $number = trim($node->field_crs_number->value);
      $id = $this->getCourseTemplateId($node);
      $course_name = trim($node->field_crs_title->value);

      if ($exception = $this->academicCrosswalk->getCourseCrosswalkException($prefix, $number)) {
        $dept = $exception->new_dept_code;
      }
      else {
        $dept = $this->academicCrosswalk->getDeptBySubject($prefix);
      }

      $parent = !empty($dept) ? $dept . '_DEPT_TMPL' : 'INST_TMPL';

      if (!empty($node->field_effective_term->entity->term_start_date->value)) {
        $start_date = $this->formatDate($node->field_effective_term->entity->term_start_date->value);
      }
      else {
        $this->getLogger('concourse_extract')->error(
          'Missing effective term start date for %tid in %title',
          [
            '%tid' => $node->field_effective_term_term->target_id ?? 'NONE',
            '%title' => $node->label(),
          ]
        );
        $start_date = '';
      }

      $output = [
        'COURSE_IDENTIFIER' => $id,
        'TITLE' => "{$prefix}-{$number} {$course_name}",
        'CAMPUS_IDENTIFIER' => 'TMPL',
        'DEPARTMENT_IDENTIFIER' => $dept,
        'START_DATE' => $start_date,
        'END_DATE' => '01/01/2099',
        'CLONE_FROM_IDENTIFIER' => $parent,
        'TIMEZONE' => $this->timezone,
        'PREFIX' => $prefix,
        'NUMBER' => $number,
        'INSTRUCTOR' => '',
        'SESSION' => '',
        'YEAR' => '',
        'CREDITS' => trim($node->field_crs_credit_hours->value),
        'DELIVERY_METHOD' => '',
        'IS_STRUCTURED' => 1,
        'IS_TEMPLATE' => 1,
        'HIDDEN_FROM_SEARCH' => 0,
      ];
      return implode('|', $output);
    }
    else {
      // This should never happen, but if it does,
      // it will be ignored as a comment in the feed file.
      return "# Could not load node $nid";
    }
  }

  /**
   * Get a single row for course extract.
   *
   * @param int $id
   *   The Course Sections ID of the HANK Section to use as the source.
   *
   * @return string
   *   A string formatted as a Concourse Course feed record.
   */
  private function getCourseRow($id): string {

    if ($section = $this->entityTypeManager->getStorage('hank_course_section')->load($id)) {

      list($session, $year) = explode(' ', $this->currentTerm->label());
      $prefix = $section->sec_subject->value;
      $number = trim($section->sec_course_no->value);
      $sec_no = $section->sec_no->value;
      $course_name = trim($section->sec_short_title->value);
      $term_id = $section->sec_term->target_id;
      $master = $this->getEffectiveMaster($prefix, $number);

      // Return an empty row for courses with no master.
      if (empty($master->id)) {
        return "";
      }

      if ($exception = $this->academicCrosswalk->getCourseCrosswalkException($prefix, $number)) {
        $dept = $exception->new_dept_code;
      }
      else {
        $dept = $this->academicCrosswalk->getDeptBySubject($prefix);
      }

      $instructor = !empty($section->sec_faculty->entity)
        ? $section->sec_faculty->entity->getName()
        : '';

      // TEMPORARY CODE to get section start/end dates.
      // @todo Move this into hank_course_sections table.
      // @see https://dvc.hfcc.net/webadmin/issues/issue8160
      if ($seats_data = $this->getCourseSectionOpenSeats($id)) {
        $start_date = $seats_data->SEC_START_DATE;
        $end_date = $seats_data->SEC_END_DATE;
      }
      else {
        $start_date = $this->formatDate($this->currentTerm->term_start_date->value);
        $end_date = $this->formatDate($this->currentTerm->term_end_date->value);
      }

      $output = [
        'COURSE_IDENTIFIER' => $id,
        'TITLE' => "{$prefix}-{$number}-{$sec_no} {$course_name} {$term_id}",
        'CAMPUS_IDENTIFIER' => $this->validateCampus($section->sec_location->value ?? ''),
        'DEPARTMENT_IDENTIFIER' => $dept,
        'START_DATE' => $start_date,
        'END_DATE' => $end_date,
        'CLONE_FROM_IDENTIFIER' => $master->id,
        'TIMEZONE' => $this->timezone,
        'PREFIX' => $prefix,
        'NUMBER' => $number,
        'INSTRUCTOR' => $instructor,
        'SESSION' => $session,
        'YEAR' => $year,
        'CREDITS' => $master->credit_hours,
        'DELIVERY_METHOD' => $section->sec_instr_methods->value,
        'IS_STRUCTURED' => 1,
        'IS_TEMPLATE' => 0,
        'HIDDEN_FROM_SEARCH' => 0,
      ];
      return implode('|', $output);
    }
    else {
      // This should never happen, but if it does,
      // it will be ignored as a comment in the feed file.
      return "# Could not load section $id";
    }
  }

  /**
   * Get course section data from HANK API.
   *
   * @param int $id
   *   The Course Sections ID.
   *
   * @return object|null
   *   Course available seats info for a section.
   */
  private function getCourseSectionOpenSeats($id) {
    $sections = &drupal_static(__FUNCTION__);
    if (!isset($sections)) {
      $tid = str_replace("/", "", $this->currentTerm->terms_id->value);
      $data = $this->hankApi->getData("course-sections-open-seats", $tid, [], TRUE);
      $sections = [];
      foreach ($data as $section) {
        $sections[$section->COURSE_SECTIONS_ID] = $section;
      }
    }
    return isset($sections[$id]) ? $sections[$id] : NULL;
  }

  /**
   * Get a single row for the section template extract.
   *
   * @param int $nid
   *   The node ID of the Catalog Course to use as the source.
   *
   * @return string
   *   A string formatted as a Concourse Section feed record.
   */
  private function getSectionTemplateRow($nid): string {
    if ($node = $this->entityTypeManager->getStorage('node')->load($nid)) {
      $id = $this->getCourseTemplateId($node);
      $output = [
        'COURSE_IDENTIFIER' => $id,
        'SECTION_IDENTIFIER' => "{$id}_SEC",
        'SECTION_LABEL' => 'Template',
      ];
      return implode('|', $output);
    }
    else {
      // This should never happen, but if it does,
      // it will be ignored as a comment in the feed file.
      return "# Could not load node $nid";
    }
  }

  /**
   * Get a single row for the section extract.
   *
   * @param int $id
   *   The Course Sections ID of the HANK Section to use as the source.
   *
   * @return string
   *   A string formatted as a Concourse Section feed record.
   */
  private function getSectionRow($id): string {
    if ($section = $this->entityTypeManager->getStorage('hank_course_section')->load($id)) {
      $prefix = $section->sec_subject->value;
      $number = trim($section->sec_course_no->value);
      $sec_no = $section->sec_no->value;

      // Return an empty row for courses with no master.
      $master = $this->getEffectiveMaster($prefix, $number);
      if (empty($master->id)) {
        return "";
      }

      $output = [
        'COURSE_IDENTIFIER' => $id,
        'SECTION_IDENTIFIER' => "{$prefix}-{$number}-{$sec_no}",
        'SECTION_LABEL' => $sec_no,
      ];
      return implode('|', $output);
    }
    else {
      // This should never happen, but if it does,
      // it will be ignored as a comment in the feed file.
      return "# Could not load section $id";
    }
  }

  /**
   * Get a single row for the description extract.
   *
   * @param int $nid
   *   The node ID of the Catalog Course to use as the source.
   *
   * @return string
   *   A string formatted as a Concourse Description feed record.
   */
  private function getDescriptionRow($nid): string {
    if ($node = $this->entityTypeManager->getStorage('node')->load($nid)) {
      $id = $this->getCourseTemplateId($node);

      $output = [
        'COURSE_IDENTIFIER' => $id,
        'DESCRIPTION' => $this->renderFieldHtml($node->field_crs_description),
        'REQUISITES' => $this->getRequisites($node),
        'NOTES' => '',
        'COMMENTS' => '',
        'IS_LOCKED' => 0,
      ];
      return implode('|', $output);
    }
    else {
      // This should never happen, but if it does,
      // it will be ignored as a comment in the feed file.
      return "# Could not load node $nid";
    }
  }

  /**
   * Get a single row for the Objective extract.
   *
   * @param int $nid
   *   The node ID of the Catalog Course to use as the source.
   *
   * @return string
   *   A string formatted as a Concourse Objective feed record.
   */
  private function getObjectiveRow($nid): string {
    if ($node = $this->entityTypeManager->getStorage('node')->load($nid)) {
      $id = $this->getCourseTemplateId($node);

      $output = [
        'COURSE_IDENTIFIER' => $id,
        'OBJECTIVES' => $this->getObjectives($node->field_course_master->entity),
        'NOTES' => '',
        'COMMENTS' => '',
        'IS_LOCKED' => 0,
      ];
      return implode('|', $output);
    }
    else {
      // This should never happen, but if it does,
      // it will be ignored as a comment in the feed file.
      return "# Could not load node $nid";
    }
  }

  /**
   * Get a single row for the Outcome extract.
   *
   * @param int $nid
   *   The node ID of the Catalog Course to use as the source.
   *
   * @return string
   *   A string formatted as a Concourse Outcome feed record.
   */
  private function getOutcomeRow($nid): string {
    if ($node = $this->entityTypeManager->getStorage('node')->load($nid)) {
      $id = $this->getCourseTemplateId($node);

      $output = [
        'COURSE_IDENTIFIER' => $id,
        'OUTCOMES' => $this->getOutcomes($node->field_course_master->entity),
        'NOTES' => '',
        'COMMENTS' => '',
        'IS_LOCKED' => 0,
      ];
      return implode('|', $output);
    }
    else {
      // This should never happen, but if it does,
      // it will be ignored as a comment in the feed file.
      return "# Could not load node $nid";
    }
  }

  /**
   * Extract requisites field output from Course Master, if any.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The Course Master node.
   *
   * @return string
   *   Requisites content or empty string.
   */
  private function getRequisites(NodeInterface $node): string {
    $output = '';
    if (!empty($node->field_crs_prq->value)) {
      $output .= '<h4>Prerequisites</h4>';
      $output .= $this->renderFieldHtml($node->field_crs_prq);
    }
    if (!empty($node->field_crs_crq->value)) {
      $output .= '<h4>Co-requisites</h4>';
      $output .= $this->renderFieldHtml($node->field_crs_crq);
    }
    return $output;
  }

  /**
   * Extract Objectives field output from Course Master, if any.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The Course Master node.
   *
   * @return string
   *   Objectives content or empty string.
   */
  private function getObjectives(NodeInterface $node): string {
    $output = '';
    if (!empty($node->field_crs_topics)) {
      $output .= '<h3>Core Course Topics</h3>';
      $output .= '<style>.syl-item-content ol ol {list-style-type: lower-alpha;}</style>';
      $content = $node->field_crs_topics->view(['label' => 'hidden']);
      $markup = $this->renderer->renderRoot($content);
      $this->sanitize($markup);
      $output .= $markup;
    }
    if (!empty($node->field_crs_lrn_objectives->value)) {
      $output .= '<h3>Core Course Learning Objectives</h3>';
      $output .= $this->renderFieldHtml($node->field_crs_lrn_objectives);
    }
    if (!empty($node->field_crs_dtl_objectives->value)) {
      $output .= '<h3>Detailed Learning Objectives</h3>';
      $output .= $this->renderFieldHtml($node->field_crs_dtl_objectives);
    }
    return $output;
  }

  /**
   * Extract Outcomes field output from Course Master, if any.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The Course Master node.
   *
   * @return string
   *   Outcomes content or empty string.
   */
  private function getOutcomes(NodeInterface $node): string {
    $output = '';
    if (!empty($node->field_crs_gened)) {
      $output .= '<h3>General Education Categories</h3>';
      $content = $node->field_crs_gened->view(['label' => 'hidden']);
      $markup = $this->renderer->renderRoot($content);
      $this->sanitize($markup);
      $output .= $markup;
    }
    if (!empty($node->field_crs_inst_outcomes->target_id)) {
      $output .= '<h3>Institutional Outcomes</h3>';
      $content = $node->field_crs_inst_outcomes->view(['label' => 'hidden']);
      $markup = $this->renderer->renderRoot($content);
      $this->sanitize($markup);
      $output .= $markup;
    }
    if (!empty($node->field_crs_mta_categories->target_id)) {
      $output .= '<h3>MTA Categories</h3>';
      $content = $node->field_crs_mta_categories->view(['label' => 'hidden']);
      $markup = $this->renderer->renderRoot($content);
      $this->sanitize($markup);
      $output .= $markup;
    }
    return $output;
  }

  /**
   * Render a field as HTML.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field to render.
   *
   * @return string
   *   HTML output reformatted into a single line,
   *   with any possible pipe characters filtered out.
   */
  private function renderFieldHtml(FieldItemListInterface $field): string {
    $content = !empty($field->first()) ? $field->first()->view() : $field->view();
    $markup = $this->renderer->renderRoot($content);
    $this->sanitize($markup);
    return $markup;
  }

  /**
   * Sanitize HTML markup.
   *
   * @param string $markup
   *   The string to sanitize.
   */
  private function sanitize(string &$markup): void {

    $markup = preg_replace(
      [
        '/\|/',
        '/' . PHP_EOL . '/',
        '/\>\s+\</m',
        '/ class=".*?"/',
        "/‘/",
        "/’/",
        '/“/',
        '/”/',
      ],
      [
        '&#124;',
        '',
        '><',
        '',
        "'",
        "'",
        '"',
        '"',
      ],
      $markup
    );

    $markup = strip_tags(
      $markup,
      '<p><br><ul><ol><li><strong><em>'
    );
  }

  /**
   * Get the effective Course Master info for a section.
   *
   * @param string $subject
   *   The course subject prefix.
   * @param string $number
   *   The course number.
   *
   * @return object|null
   *   An object containing the relevant information.
   */
  private function getEffectiveMaster($subject, $number): object {
    $course_name = "{$subject}-{$number}";
    if (isset(static::$masters[$course_name])) {
      return static::$masters[$course_name];
    }

    $nodeStorage = $this->entityTypeManager->getStorage('node');

    $output = new \StdClass();

    $nids = $nodeStorage->getQuery()
      ->condition('field_crs_subject', $subject)
      ->condition('field_crs_number', $number)
      ->condition('type', 'course_master')
      ->accessCheck(FALSE)
      ->execute();

    if (!$nids) {
      return $output;
    }

    $nid = reset($nids);
    $master = clone $nodeStorage->load($nid);

    $effective_start_date = $master->field_effective_term->entity->term_start_date->value;

    // If the master effective term is later than the term being generated,
    // walk backwards through revisions until we find the most recent revision
    // that is effective on or before the current term start date.
    // Note: This is still useful after #8571 to not send some changes too early.
    if ($effective_start_date > $this->currentTerm->term_start_date) {
      $revisions = $nodeStorage->revisionIds($master);
      rsort($revisions);
      foreach ($revisions as $vid) {
        $revision = $nodeStorage->loadRevision($vid);
        $effective_start_date = $revision->field_effective_term->entity->term_start_date->value;
        if ($effective_start_date <= $this->currentTerm->term_start_date) {
          $master = clone $revision;
          break;
        }
      }
    }

    $output->id = $this->getCourseTemplateId($master);
    $output->credit_hours = $master->field_crs_credit_hours->value;
    $output->effective_term = $master->field_effective_term->target_id;

    static::$masters[$course_name] = $output;
    return $output;
  }

  /**
   * Validate section campus.
   *
   * @param string $campus
   *   A campus identifier.
   *
   * @return string
   *   The specified campus ID if valid, or "Other".
   *
   * @see Drupal\options\Plugin\Field\FieldType\ListItemBase::allowedValuesString()
   */
  private function validateCampus(string $campus): string {

    static $valid_keys;
    if (empty($valid_keys)) {
      $list = $this->extractCampus([]);
      $list = explode(PHP_EOL, $list);
      $list = array_map('trim', $list);
      $list = array_filter($list, 'strlen');
      foreach ($list as $row) {
        list($key, $value) = preg_split('/\|/', $row);
        $valid_keys[$key] = $value;
      }
    }
    return isset($valid_keys[$campus]) ? $campus : 'Other';
  }

  /**
   * Get the ID for a CourseTemplate row from node properties.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The source node.
   *
   * @return string
   *   The generated ID.
   */
  private function getCourseTemplateId(NodeInterface $node): string {
    $prefix = $node->field_crs_subject->target_id;
    $number = trim($node->field_crs_number->value);
    return "{$prefix}-{$number}_TMPL";
  }

  /**
   * Get all currently active Catalog Course node ids.
   *
   * @param array $options
   *   Command-line options passed from Drush Commands service.
   *
   * @return array
   *   An array of nids.
   */
  private function getCurrentCourseNids(array $options): array {
    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', 'catalog_course')
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('field_inactive', 1, "<>")
      ->accessCheck(FALSE)
      ->sort('title');

    if (!empty($options['school'])) {
      $prefixes = $this->academicCrosswalk->getSubjectsBySchool($options['school']);
      $query->condition('field_crs_subject', $prefixes, 'IN');
    }

    if (!empty($options['prefix'])) {
      $prefix = explode(',', $options['prefix']);
      $query->condition('field_crs_subject', $prefix, 'IN');
    }

    if (!empty($options['number'])) {
      $query->condition('field_crs_number', $options['number']);
    }

    return $query->execute();
  }

  /**
   * Get course sections ids for a specified term.
   *
   * @param array $options
   *   Command-line options passed from Drush Commands service.
   *
   * @return array
   *   An array of section ids.
   */
  private function getCourseSectionsIds(array $options): array {

    $query = $this->entityTypeManager->getStorage('hank_course_section')->getQuery()
      ->condition('sec_status', ['A', 'N'], "IN")
      ->condition('sec_term', $options['term'])
      ->accessCheck(FALSE)
      ->sort('sec_subject')
      ->sort('sec_course_no')
      ->sort('sec_no');

    if (!empty($options['school'])) {
      $prefixes = $this->academicCrosswalk->getSubjectsBySchool($options['school']);
      $query->condition('sec_subject', $prefixes, 'IN');
    }

    if (!empty($options['prefix'])) {
      $prefix = explode(',', $options['prefix']);
      $query->condition('sec_subject', $prefix, 'IN');
    }

    if (!empty($options['number'])) {
      $query->condition('sec_course_no', $options['number']);
    }

    return $query->execute();
  }

  /**
   * Get contents of School extract as an array.
   *
   * @return array
   *   An array of Schools values.
   */
  private function getSchoolOpts(): array {
    $schools = [];
    foreach (explode(PHP_EOL, $this->extractSchool([])) as $row) {
      $row = trim($row);
      if (strpos($row, '|')) {
        [$key, $value] = explode('|', $row);
        $schools[$key] = $value;
      }
    }
    return $schools;
  }

  /**
   * Standardize date formatting for feeds.
   *
   * @param int $date
   *   The UNIX timestamp date to be formatted.
   *
   * @return string
   *   A formatted date.
   */
  private function formatDate(int $date): string {
    return $this->dateFormatter->format($date, 'custom', 'm/d/Y');
  }

}
