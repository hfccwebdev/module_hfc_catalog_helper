<?php

namespace Drupal\hfc_catalog_helper;

/**
 * Defines the Concourse Data Feeds Interface.
 *
 * @package Drupal\hfc_catalog_helper
 */
interface ConcourseDataFeedsInterface {

  /**
   * Extract Concourse Data Feed for testing before upload.
   *
   * @param string $feedName
   *   The Catalog Year to archive.
   * @param array $options
   *   Command-line options passed from Drush Commands service.
   *
   * @return string
   *   Returns a big string.
   */
  public function extract($feedName, array $options = []);

  /**
   * Upload Concourse Data Feed.
   *
   * @param string $feedName
   *   The Catalog Year to archive.
   * @param array $options
   *   Command-line options passed from Drush Commands service.
   *
   * @return bool
   *   Returns TRUE if no errors were encountered.
   */
  public function upload($feedName, array $options = []);

}
