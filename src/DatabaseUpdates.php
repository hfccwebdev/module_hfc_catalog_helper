<?php

namespace Drupal\hfc_catalog_helper;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hfc_related_program\Entity\RelatedProgram;
use Drupal\node\Entity\Node;
use Drupal\pathauto\PathautoState;

/**
 * This class contains one-shot routines related to module updates.
 *
 * Each method should be disabled after being executed in production.
 */
class DatabaseUpdates implements DatabaseUpdatesInterface {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\hfc_catalog_helper\CatalogUtilitiesInterface definition.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $catalogHelper;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Constructs a new NewCourseSections object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   TheAccount Proxy service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $hfc_catalog_helper
   *   The Catalog Helper service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    CatalogUtilitiesInterface $hfc_catalog_helper,
    TimeInterface $time
  ) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->catalogHelper = $hfc_catalog_helper;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function runUpdate(string $updateName, array $options = []): void {
    $this->checkUid();
    if (method_exists($this, $updateName)) {
      $this->getLogger('hfc_catalog_helper')->notice(
        'User %uid (%login) executing database update %name.',
        [
          '%uid' => $this->currentUser->id(),
          '%login' => $this->currentUser->getDisplayName(),
          '%name' => $updateName,
        ]
      );
      $this->$updateName($options);
    }
  }

  /**
   * Bulk save operations.
   *
   * Bulk load and save all nodes of a given type
   * to recompose computed field values.
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue4858
   */
  private function catalogBulkSave(array $options) {
    if (empty($options['bundle'])) {
      $this->messenger()->addMessage($this->t(
        'You must specify a content type using the --bundle flag.'));
      return;
    }
    if (empty($options['issue'])) {
      $this->messenger()->addMessage($this->t(
        'You must specify a related Web Support issue using the --issue flag.'));
      return;
    }

    $nids = $this->catalogHelper->getNidsByType($options['bundle']);

    $this->messenger()->addMessage($this->t(
      'Found @count qualifying @types.', [
        '@count' => count($nids),
        '@type' => str_replace('_', ' ', $options['bundle']),
      ])
    );

    foreach ($nids as $nid) {
      $node = Node::load($nid);
      $node->setNewRevision(TRUE);
      $node->setRevisionCreationTime($this->time->getCurrentTime());
      $node->setRevisionUserId($options['uid']);
      $node->setRevisionLogMessage($this->t(
        'Issue #@issue: Bulk update of @types to recompose computed fields.', [
          '@issue' => $options['issue'],
          '@type' => str_replace('_', ' ', $options['bundle']),
        ])
      );
      $node->save();
    }
  }

  /**
   * Populate Supplemental Course Info reference
   * to all Course Masters and Catalog Courses.
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue5919
   */
  private function suppCourseRelink() {

    $nids = $this->catalogHelper->getNidsByType('course_master');

    foreach ($nids as $nid) {
      $master = Node::load($nid);
      if (!empty($master->field_supplemental_info->target_id)) {
        $supplemental = $master->field_supplemental_info->entity;
      }
      else {
        if ($supplemental = $this->catalogHelper->getSupplementalCourseInfo($nid)) {
          $master->field_supplemental_info->target_id = $supplemental->id();
          $master->field_supplemental_info->entity = $supplemental;

          $master->setNewRevision(TRUE);
          $master->setRevisionCreationTime($this->time->getRequestTime());
          $master->setRevisionUserId($this->currentUser->id());
          $master->setRevisionLogMessage('Set field_supplemental_info to ' . $supplemental->id() . '.');
          $master->save();
          echo "{$master->label()} ($nid): Added supplemental info for course master." . PHP_EOL;
        }
        elseif (($master->field_inactive->value <> 1) && empty($master->field_deactivation_date->value)) {
          echo "{$master->label()} ($nid): Could not load supplemental info for course master." . PHP_EOL;
        }
      }

      if (!empty($supplemental) && $catalog = $this->catalogHelper->getCatalogCourse($master->id())) {
        if (empty($catalog->field_supplemental_info->target_id)) {
          $catalog->field_supplemental_info->target_id = $supplemental->id();
          $catalog->field_supplemental_info->entity = $supplemental;

          $catalog->setNewRevision(TRUE);
          $catalog->setRevisionCreationTime($this->time->getRequestTime());
          $catalog->setRevisionUserId($this->currentUser->id());
          $catalog->setRevisionLogMessage('Set field_supplemental_info to ' . $supplemental->id() . '.');
          $catalog->save();
          echo "{$catalog->label()} ($catalog->id()): Added supplemental info for catalog course." . PHP_EOL;
        }
      }
      usleep('500');
    }
  }

  /**
   * Populate Supplemental Program Info reference
   * to all Program Masters and Catalog Programs.
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue5320
   */
  private function suppProgramRelink() {

    $nids = $this->catalogHelper->getNidsByType('program_master');

    foreach ($nids as $nid) {
      $master = Node::load($nid);
      if (!empty($master->field_supplemental_info->target_id)) {
        $supplemental = $master->field_supplemental_info->entity;
      }
      else {
        if ($supplemental = $this->catalogHelper->getSupplementalProgramInfo($nid)) {
          $master->field_supplemental_info->target_id = $supplemental->id();
          $master->field_supplemental_info->entity = $supplemental;

          $master->setNewRevision(TRUE);
          $master->setRevisionCreationTime($this->time->getRequestTime());
          $master->setRevisionUserId($this->currentUser->id());
          $master->setRevisionLogMessage('Set field_supplemental_info to ' . $supplemental->id() . '.');
          $master->save();
          echo "{$master->label()} ($nid): Added supplemental info for program master." . PHP_EOL;
        }
        elseif ($master->field_inactive->value <> 1) {
          echo "{$master->label()} ($nid): Could not load supplemental info for program master." . PHP_EOL;
        }
      }

      if (!empty($supplemental) && $catalog = $this->catalogHelper->getCatalogProgram($master->id())) {
        if (empty($catalog->field_supplemental_info->target_id)) {
          $catalog->field_supplemental_info->target_id = $supplemental->id();
          $catalog->field_supplemental_info->entity = $supplemental;

          $catalog->setNewRevision(TRUE);
          $catalog->setRevisionCreationTime($this->time->getRequestTime());
          $catalog->setRevisionUserId($this->currentUser->id());
          $catalog->setRevisionLogMessage('Set field_supplemental_info to ' . $supplemental->id() . '.');
          $catalog->save();
          echo "{$catalog->label()} ($catalog->id()): Added supplemental info for catalog program." . PHP_EOL;
        }
      }
      usleep('500');
    }
  }

  /**
   * Populate Supplemental Course Info Office Contact Info.
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue5279
   */
  private function suppCourseContactUpdate() {

    $schools = $this->subjectSchoolContacts();

    $nids = $this->catalogHelper->getNidsByType('supplemental_course_info');

    foreach ($nids as $nid) {
      $supplemental = Node::load($nid);

      if (!empty($supplemental->field_course_master->target_id)) {
        $master = $supplemental->field_course_master->entity;
        if ($master->field_inactive->value) {
          continue;
        }
        $subject = $master->field_crs_subject->target_id;
        $school = (!empty($subject) && !empty($schools[$subject])) ? $schools[$subject] : NULL;
        if (!empty($school)) {
          if ((empty($supplemental->field_office_contact->target_id)) || ($supplemental->field_office_contact->target_id <> $school)) {
            $supplemental->field_office_contact->setValue($school);
            $supplemental->setNewRevision(TRUE);
            $supplemental->setRevisionCreationTime($this->time->getRequestTime());
            $supplemental->setRevisionUserId($this->currentUser->id());
            $supplemental->setRevisionLogMessage("Set field_office_contact to {$school}.");
            $supplemental->save();
            echo $supplemental->label() . " " . $school . PHP_EOL;
          }
        }
        else {
          echo $supplemental->label() . " no school info found for $subject." . PHP_EOL;
        }
      }
      usleep('500');
    }
  }

  /**
   * One-time batch to populate field_acad_level.
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue5318
   */
  private function fieldAcadLevel() {

    $nids = $this->catalogHelper->getNidsByType($this->curriculumTypes());

    foreach ($nids as $nid) {
      $node = Node::load($nid);
      if (empty($node->field_acad_level->value)) {
        echo $this->t("@id @label (@type)", ["@id" => $node->id(), "@label" => $node->label(), "@type" => $node->getType()]) . PHP_EOL;
        $node->field_acad_level->setValue("UG");
        $node->setNewRevision(TRUE);
        $node->setRevisionCreationTime($this->time->getRequestTime());
        $node->setRevisionUserId($this->currentUser->id());
        $node->setRevisionLogMessage('Set default value for academic level.');
        $node->save();
      }
    }
  }

  /**
   * One-time batch to populate field_employer_sponsored.
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue6849
   */
  private function fieldEmployerSponsored() {

    $nids = $this->catalogHelper->getNidsByType(['program_proposal', 'program_master', 'catalog_program']);

    foreach ($nids as $nid) {
      $node = Node::load($nid);
      if (
          !($node->getType() == 'program_proposal' && $node->field_proposal_processed->value) &&
          ($node->field_program_status->value !== 'inactive') &&
          is_null($node->field_employer_sponsored->value)
        ) {
        echo $this->t("@id @label (@type)", ["@id" => $node->id(), "@label" => $node->label(), "@type" => $node->getType()]) . PHP_EOL;
        $node->field_employer_sponsored->setValue(FALSE);
        $node->setNewRevision(TRUE);
        $node->setRevisionCreationTime($this->time->getRequestTime());
        $node->setRevisionUserId($this->currentUser->id());
        $node->setRevisionLogMessage('Issue #6849: Set default value for employer sponsored.');
        $node->save();
      }
    }
  }

  /**
   * Verify that field_inactive is set for all Course Masters where field_deactivation_date is set.
   */
  private function courseDeactivationCheck() {
    $nids = $this->catalogHelper->getNidsByType('course_master');

    foreach ($nids as $nid) {
      $master = Node::load($nid);

      if (!empty($master->field_deactivation_date->value) && ($master->field_inactive->value <> 1)) {
        $master->field_inactive->setValue(TRUE);
        $master->setNewRevision(TRUE);
        $master->setRevisionCreationTime($this->time->getRequestTime());
        $master->setRevisionUserId($this->currentUser->id());
        $master->setRevisionLogMessage('Set field_inactive to to reflect deactivation date.');
        $master->save();
        echo "{$master->label()} ($nid): set to inactive." . PHP_EOL;
      }
      usleep('500');
    }
  }

  /**
   * Populate missing field_program_master values for completed program_proposal nodes.
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue6587
   */
  private function programProposalMissingMasters() {
    $conditions = [
      ['field' => 'field_proposal_processed', 'value' => 1],
      ['field' => 'field_program_master', 'value' => 'notExists'],
    ];

    if ($nids = $this->catalogHelper->getNidsByType('program_proposal', $conditions)) {

      $updateProposal = function ($proposal) {
        echo "Proposal {$proposal->id()}: {$proposal->label()} ({$proposal->field_program_code->value})";

        $conditions = [['field' => 'field_program_code', 'value' => $proposal->field_program_code->value]];

        if ($nids = $this->catalogHelper->getNidsByType('supplemental_program_info', $conditions)) {
          $supplemental = Node::load(reset($nids));
          if ($master = $supplemental->field_program_master->entity) {
            echo " {$master->id()}";

            $proposal->field_program_master->target_id = $master->id();
            $proposal->field_program_master->entity = $master;

            $proposal->setNewRevision(TRUE);
            $proposal->setRevisionCreationTime($this->time->getRequestTime());
            $proposal->setRevisionUserId($this->currentUser->id());
            $proposal->setRevisionLogMessage("Batch update: added missing field_program_master value {$master->id()}.");
            $proposal->save();
          }
          else {
            echo " **SUPP INFO {$supplemental->id()} HAS MISSING MASTER**";
          }
        }
        else {
          echo " **NO MASTER FOUND**";
        }
        echo PHP_EOL;
      };

      $proposals = Node::loadMultiple($nids);
      array_walk($proposals, $updateProposal);
    }
  }

  /**
   * Populate field_acyr_list for Program Sequences whose titles end in discernible years.
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue7037
   */
  private function programSequencePopulateAcyr() {
    $conditions = [
      ['field' => 'field_acyr_list', 'value' => 'notExists'],
    ];

    if ($nids = $this->catalogHelper->getNidsByType('program_sequence', $conditions)) {

      $doUpdate = function ($sequence) {

        $acyr = FALSE;

        foreach ([
          2017 => '2016-2017',
          2018 => '2017-2018',
          2019 => '2018-2019',
          2020 => '2019-2020',
        ] as $year => $value) {
          if (strpos($sequence->label(), $value) > 0) {
            $acyr = $year;
          }
        }

        if ($acyr) {
          echo " {$sequence->label()}: Set to $acyr";
          $sequence->field_acyr_list->setValue($acyr);
          $sequence->setNewRevision(TRUE);
          $sequence->setRevisionCreationTime($this->time->getRequestTime());
          $sequence->setRevisionUserId($this->currentUser->id());
          $sequence->setRevisionLogMessage("Issue #7037: bulk update sequences to add academic year.");
          $sequence->save();
          echo PHP_EOL;
        }
      };

      $sequences = Node::loadMultiple($nids);
      array_walk($sequences, $doUpdate);
    }
  }

  /**
   * Issue #7497 mass-update department code (from numeric to alpha).
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue7497
   */
  private function fixNumericDepartmentCodes() {
    $conditions = [
      ['field' => 'field_department', 'value' => '^[0-9]', 'operator' => 'REGEXP'],
    ];
    if ($nids = $this->catalogHelper->getNidsByType($this->curriculumTypes(), $conditions)) {

      $updateDepartment = function ($nid) {

        $departments = $this->convertDepartmentCodes();

        $node = Node::load($nid);
        $dept_old = $node->field_department->target_id;
        $dept_new = !empty($departments[$node->field_department->target_id]) ? $departments[$node->field_department->target_id] : NULL;

        $program_code = $node->field_program_code->value ?: "";
        $title = trim("$program_code {$node->label()}");

        echo $this->t('@title (@type) @dept_name: @old => @new', [
          '@title' => $title,
          '@type' => $node->getType(),
          '@dept_name' => $node->field_department->entity->label(),
          '@old' => $node->field_department->target_id,
          '@new' => $dept_new ?: '**NONE**',
        ]) . PHP_EOL;

        if (!empty($dept_new)) {

          $node->field_department->target_id = $dept_new;

          $node->setNewRevision(TRUE);
          $node->setRevisionCreationTime($this->time->getRequestTime());
          $node->setRevisionUserId($this->currentUser->id());
          $node->setRevisionLogMessage("Issue #7497: Bulk update convert numeric department code to preferred mnemonic.");
          $node->save();

          usleep(500);
        }

      };

      array_map($updateDepartment, $nids);
    }
  }

  /**
   * Move Program Code from Supplemental to Master.
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue7950
   */
  private function moveProgramCodeToMaster() {

    if ($nids = $this->catalogHelper->getNidsByType('program_master')) {

      $doUpdate = function ($nid) {
        $master = Node::load($nid);
        $supplemental = $this->catalogHelper->getSupplementalProgramInfo($master);

        if (empty($supplemental)) {
          echo "{$master->label()} ({$master->id()}): NO SUPPLEMENTAL INFO FOUND." . PHP_EOL;
          return;
        }

        if (
          empty($master->field_program_code->getValue()) &&
          !empty($supplemental->field_program_code->getValue())
        ) {
          $master->field_program_code->setValue($supplemental->field_program_code->getValue());

          $master->setNewRevision(TRUE);
          $master->setRevisionCreationTime($this->time->getRequestTime());
          $master->setRevisionUserId($this->currentUser->id());
          $master->setRevisionLogMessage("Issue #7950: Bulk update to copy program code from Supplemental Info to Master.");
          $master->save();
          echo "{$master->label()} ({$master->id()}): Updated." . PHP_EOL;
        }
        else {
          echo "{$master->label()} ({$master->id()}): no change." . PHP_EOL;
        }
      };

      array_map($doUpdate, $nids);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkUid() {
    if ($this->currentUser->id() == 0) {
      die("Cannot run as user 0!" . PHP_EOL);
    }
  }

  /**
   * Define all curriculum content types.
   */
  private function curriculumTypes() {
    return ['course_proposal', 'course_master', 'catalog_course', 'program_proposal', 'program_master', 'catalog_program'];
  }

  /**
   * Issue #7497 map GL dept code to alpha.
   */
  private function convertDepartmentCodes() {
    return [
      '1101' => 'COMM',
      '1103' => 'FORL',
      '1109' => 'TECM',
      '1201' => 'HUMS',
      '1202' => 'ARTS',
      '1203' => 'HPED',
      '1204' => 'THEA',
      '1205' => 'PHIL',
      '1206' => 'CERM',
      '1207' => 'MUSI',
      '1209' => 'BLDI',
      '1301' => 'MATH',
      '1302' => 'MATH',
      '1400' => 'BIOL',
      '1402' => 'ASTR',
      '1403' => 'BIOL',
      '1404' => 'CHEM',
      '1405' => 'ENGR',
      '1503' => 'CRJU',
      '1504' => 'GEOG',
      '1505' => 'HIST',
      '1507' => 'PSYC',
      '1508' => 'RELG',
      '1509' => 'SOCI',
      '1800' => 'FORL',
      '1805' => 'EDUC',
      '2101' => 'BADM',
      '2102' => 'ACCT',
      '2103' => 'BADM',
      '2105' => 'BCAP',
      '2108' => 'MGMT',
      '2200' => 'HTCR',
      '2207' => 'MADP',
      '2208' => 'MRBP',
      '2210' => 'PHTP',
      '2211' => 'PTAP',
      '2212' => 'RADP',
      '2213' => 'RTHP',
      '2214' => 'SRGP',
      '2215' => 'OPHT',
      '2300' => 'NSGD',
      '2302' => 'NSGD',
      '2402' => 'BLDE',
      '2406' => 'TAED',
      '2503' => 'AUTO',
      '2504' => 'AUSV',
      '2405' => 'TAMN',
      '2501' => 'TECH',
      '2502' => 'BLDA',
      '2506' => 'CISP',
      '2508' => 'ELEC',
      '2509' => 'HOSP',
      '2510' => 'MFPS',
    ];
  }

  /**
   * Issue #8399 A one-time copy of field_program_goals
   * and field_program_mission values from supplemental_program_info
   * to program_master and catalog_programs content type.
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue8399
   */
  private function copyNeededSupProgramInfo() {
    $nids = $this->catalogHelper->getNidsByType('program_master');

    foreach ($nids as $nid) {
      $master = Node::load($nid);
      $supplemental = $this->catalogHelper->getSupplementalProgramInfo($master);

      if (empty($supplemental)) {
        echo "{$master->label()} ({$master->id()}): NO SUPPLEMENTAL INFO FOUND." . PHP_EOL;
      }
      else {
        $updated = FALSE;

        foreach ([
          'field_program_mission',
          'field_program_goals',
        ] as $fieldname) {
          if (empty($master->{$fieldname}->getValue()) && !empty($supplemental->{$fieldname}->getValue())) {
            $master->{$fieldname}->setValue($supplemental->{$fieldname}->getValue());
            $updated = TRUE;
          }
        }

        if ($updated) {
          $master->setNewRevision(TRUE);
          $master->setRevisionCreationTime($this->time->getRequestTime());
          $master->setRevisionUserId($this->currentUser->id());
          $master->setRevisionLogMessage("Issue #8399 A one-time copy of field_program_goals and field_program_mission values from supplemental_program_info to program_master and catalog_programs content type");
          $master->save();
          echo "{$master->label()} ({$master->id()}): Updated." . PHP_EOL;
        }
        else {
          echo "{$master->label()} ({$master->id()}): no change." . PHP_EOL;
        }
      }
      usleep('5000');
    }

    echo "Finished!" . PHP_EOL;
  }

  /**
   * Issue #8400 A one-time copy of:
   *
   * field_catalog_program: Program Sequence, Pseudo Program, Articulation Agreement
   * field_program_master: Assessment: Program PLO, Assessment: Program ILO, Related Training Instruction
   *
   * to field_related_program in the same content types.
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue8400
   */
  private function copyNeededRelatedProgramInfo() {

    $nids = $this->catalogHelper->getNidsByType([
      'articulation_agreement',
      'program_assessment_annual_report',
      'program_assessment_institutional',
      'program_sequence',
      'pseudo_program',
      'rti',
    ]);

    // This can be used to look up RelatedProgram entities when
    // content points to proposals that are no longer current.
    $altProposalCheck = function($nid) {
      $node = Node::load($nid);
      if ($node->getType() == 'program_proposal') {
        if (!empty($node->field_program_master->target_id)) {
          return RelatedProgram::lookup('master_nid', $node->field_program_master->target_id);
        }
      }
    };

    foreach ($nids as $nid) {
      $node = Node::load($nid);

      $field_values = NULL;

      if (!is_object($node->field_related_program) || empty($node->field_related_program->getValue())) {
        if (is_object($node->field_catalog_program) && !empty($node->field_catalog_program->getValue())) {
          foreach ($node->field_catalog_program->getValue() as $item) {
            if ($connector = RelatedProgram::lookup('catalog_nid', $item['target_id'])) {
              $field_values[] = ['target_id' => $connector->id()];
            }
            elseif ($connector = RelatedProgram::lookup('proposal_nid', $item['target_id'])) {
              $field_values[] = ['target_id' => $connector->id()];
            }
            elseif ($connector = $altProposalCheck($item['target_id'])) {
              $field_values[] = ['target_id' => $connector->id()];
            }
            else {
              echo "COULD NOT MATCH CATALOG NID {$item['target_id']}!!" . PHP_EOL;
            }
          }
        }
        elseif (is_object($node->field_program_master) && !empty($node->field_program_master->getValue())) {
          foreach ($node->field_program_master->getValue() as $item) {
            if ($connector = RelatedProgram::lookup('master_nid', $item['target_id'])) {
              $field_values[] = ['target_id' => $connector->id()];
            }
            elseif ($connector = RelatedProgram::lookup('proposal_nid', $item['target_id'])) {
              $field_values[] = ['target_id' => $connector->id()];
            }
            elseif ($connector = $altProposalCheck($item['target_id'])) {
              $field_values[] = ['target_id' => $connector->id()];
            }
            else {
              echo "COULD NOT MATCH MASTER NID {$item['target_id']}!!" . PHP_EOL;
            }
          }
        }

        if (!empty($field_values)) {
          $node->field_related_program->setValue($field_values);
          $node->setNewRevision(TRUE);
          $node->setRevisionCreationTime($this->time->getRequestTime());
          $node->setRevisionUserId($this->currentUser->id());
          $node->setRevisionLogMessage("Issue #8400 Populate new field_related_program from existing fields.");
          $node->save();
          echo "{$node->label()} ({$node->id()}): Updated." . PHP_EOL;
          usleep(5000);
        }
        else {
          echo "{$node->label()} ({$node->id()}): no change." . PHP_EOL;
        }
      }
    }
    echo "Finished!" . PHP_EOL;
  }

  /**
   * Issue #6633 Force update pathauto for a provided content type.
   */
  private function contentPathautoCreate(array $options) {
    if (!isset($options['bundle'])) {
      $this->messenger()->addError($this->t('Please specify a content type with --bundle option.'));
      return;
    }
    $nids = $this->catalogHelper->getNidsByType($options['bundle']);

    foreach ($nids as $nid) {
      $node = Node::load($nid);
      $node->path->pathauto = PathautoState::CREATE;
      $node->save();
    }
  }

  /**
   * Issue #6633 Force update pathauto for all users.
   */
  public function userPathautoCreate() {
    $userStorage = $this->entityTypeManager->getStorage('user');
    $uids = $userStorage->getQuery()
      ->condition('status', 1)
      ->accessCheck(FALSE)
      ->execute();

    foreach ($uids as $uid) {
      $user = $userStorage->load($uid);
      $user->path->pathauto = PathautoState::CREATE;
      $user->save();
      \Drupal::service('ldap.drupal_user_processor')->reset();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function subjectSchoolContacts() {

    $offices = [
      'BEPD' => 8585,
      'HHS' => 8586,
      'LA' => 8587,
      'STEM' => 8588,
      'COUN' => 5712,
      'PLMT' => 2716,
    ];

    $schools = $this->subjectSchools();

    // Add rewrites for co-op courses to PLMT.
    // Note: Do not auto-update COMM.
    // Co-op courses go to PLMT, the rest go to LA.
    $schools["ICO"] = 'PLMT';
    $schools["SCO"] = 'PLMT';
    $schools["SSCO"] = 'PLMT';

    $map = [];
    foreach ($schools as $subject => $school) {
      $map[$subject] = $offices[$school];
    }
    return $map;
  }

  /**
   * {@inheritdoc}
   */
  public function subjectSchools() {

    return [
      "ACT" => 'BEPD',
      "AUTO" => 'BEPD',
      "BAC" => 'BEPD',
      "BBA" => 'BEPD',
      "BCA" => 'BEPD',
      "BCO" => 'BEPD',
      "BEC" => 'BEPD',
      "BFN" => 'BEPD',
      "BLDA" => 'BEPD',
      "BLDB" => 'BEPD',
      "BLDE" => 'BEPD',
      "BLDI" => 'BEPD',
      "BLDP" => 'BEPD',
      "BLDR" => 'BEPD',
      "BLW" => 'BEPD',
      "BMA" => 'BEPD',
      "BSC" => 'BEPD',
      "CDL" => 'BEPD',
      "CEHFS" => 'BEPD',
      "CIMEL" => 'BEPD',
      "CIMHP" => 'BEPD',
      "CIMMT" => 'BEPD',
      "CIMPR" => 'BEPD',
      "CIMTA" => 'BEPD',
      "CIMWD" => 'BEPD',
      "CIS" => 'BEPD',
      "CNT" => 'BEPD',
      "DRAF" => 'BEPD',
      "ELEC" => 'BEPD',
      "ENT" => 'BEPD',
      "HOSP" => 'BEPD',
      "INTR" => 'BEPD',
      "MFMT" => 'BEPD',
      "MGT" => 'BEPD',
      "MTT" => 'BEPD',
      "MTWD" => 'BEPD',
      "PEFT" => 'BEPD',
      "PLGL" => 'BEPD',
      "PLMB" => 'BEPD',
      "REEN" => 'BEPD',
      "TADV" => 'BEPD',
      "TAEL" => 'BEPD',
      "TAFD" => 'BEPD',
      "TAFP" => 'BEPD',
      "TAGD" => 'BEPD',
      "TAIM" => 'BEPD',
      "TAMA" => 'BEPD',
      "TAMJ" => 'BEPD',
      "TAMN" => 'BEPD',
      "TAMT" => 'BEPD',
      "TAPI" => 'BEPD',
      "TAPP" => 'BEPD',
      "TAPT" => 'BEPD',
      "TASM" => 'BEPD',
      "TECH" => 'BEPD',
      "WFPD" => 'BEPD',

      "AH" => 'HHS',
      "CHD" => 'HHS',
      "EDU" => 'HHS',
      "EMS" => 'HHS',
      "HCS" => 'HHS',
      "HIT" => 'HHS',
      "HPE" => 'HHS',
      "HPEA" => 'HHS',
      "MII" => 'HHS',
      "MIS" => 'HHS',
      "MOA" => 'HHS',
      "NCS" => 'HHS',
      "NSG" => 'HHS',
      "OPT" => 'HHS',
      "PHT" => 'HHS',
      "PTA" => 'HHS',
      "RAD" => 'HHS',
      "RTH" => 'HHS',
      "SRG" => 'HHS',

      "ANTH" => 'LA',
      "ARA" => 'LA',
      "ART" => 'LA',
      "ASL" => 'LA',
      "CHN" => 'LA',
      "CRJ" => 'LA',
      "DNCA" => 'LA',
      "ELI" => 'LA',
      "ENG" => 'LA',
      "FRE" => 'LA',
      "GEOG" => 'LA',
      "GER" => 'LA',
      "HIST" => 'LA',
      "HON" => 'LA',
      "HUM" => 'LA',
      "ITAL" => 'LA',
      "JOUR" => 'LA',
      "MUS" => 'LA',
      "NCELI" => 'LA',
      "PHIL" => 'LA',
      "POLS" => 'LA',
      "PSY" => 'LA',
      "SOC" => 'LA',
      "SPC" => 'LA',
      "SPN" => 'LA',
      "SSC" => 'LA',
      "TCM" => 'LA',
      "THEA" => 'LA',
      "VTL" => 'LA',
      "WR" => 'LA',

      "ASTR" => 'STEM',
      "ATMS" => 'STEM',
      "BIO" => 'STEM',
      "CHEM" => 'STEM',
      "ENGR" => 'STEM',
      "ENGT" => 'STEM',
      "GEOL" => 'STEM',
      "GIS" => 'STEM',
      "MATH" => 'STEM',
      "MIT" => 'STEM',
      "PHYS" => 'STEM',
      "PSCI" => 'STEM',
      "SCI" => 'STEM',

      "COLL" => 'COUN',
      "COUN" => 'COUN',
    ];
  }
}
