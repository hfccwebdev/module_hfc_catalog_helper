<?php

namespace Drupal\hfc_catalog_helper;

use Drupal\Core\Database\Connection;

/**
 * Defines the HFC Academic Crosswalk Service.
 *
 * @package Drupal\hfc_catalog_helper
 */
class AcademicCrosswalk implements AcademicCrosswalkInterface {

  /**
   * Stores the Database Connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllCrosswalkData() {
    return $this->database->query("SELECT * FROM {hfc_academic_crosswalk}")->fetchAll();
  }

  /**
   * {@inheritdoc}
   */
  public function getAllDepts() {
    return $this->database->query("SELECT new_school_code AS school, new_dept_code AS dept, new_dept_name AS name
      FROM {hfc_academic_crosswalk} WHERE new_dept_code != 'N/A'
      GROUP BY new_school_code, new_dept_code, new_dept_name ORDER BY new_school_code, new_dept_code"
    )->fetchAll();
  }

  /**
   * {@inheritdoc}
   */
  public function getSchoolByDept($dept) {
    $result = $this->database->query("SELECT new_school_code AS school
      FROM {hfc_academic_crosswalk} WHERE new_dept_code = :dept
      GROUP BY new_school_code",
      [':dept' => $dept]
    )->fetchCol();
    return !empty($result) ? reset($result) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeptBySubject($prefix) {
    $result = $this->database->query("SELECT new_dept_code AS dept
      FROM {hfc_academic_crosswalk} WHERE prefix_code = :prefix
      GROUP BY new_dept_code",
      [':prefix' => $prefix]
    )->fetchCol();
    return !empty($result) ? reset($result) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubjectsBySchool($school) {
    $result = $this->database->query("SELECT prefix_code AS prefix
      FROM {hfc_academic_crosswalk}
      WHERE new_school_code = :school AND prefix_code <> 'N/A'
      GROUP BY prefix_code ORDER BY prefix_code",
      [':school' => $school]
    )->fetchCol();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubjectDepts() {
    $result = $this->database->query("
      SELECT field_crs_subject_target_id AS prefix, new_dept_code AS dept
      FROM {node__field_crs_subject} subject
      JOIN {node__field_inactive} inactive
      ON (subject.revision_id = inactive.revision_id AND subject.bundle = 'catalog_course')
      LEFT JOIN {hfc_academic_crosswalk} crosswalk
      ON (subject.field_crs_subject_target_id = crosswalk.prefix_code)
      WHERE field_inactive_value <> 1
      GROUP BY field_crs_subject_target_id, new_dept_code ORDER BY field_crs_subject_target_id"
    )->fetchAllAssoc('prefix');
    return array_map(function ($subject) {
      return $subject->dept;
    }, $result);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubjectMissingDept() {
    $result = $this->database->query("
      SELECT field_crs_subject_target_id AS prefix
      FROM {node__field_crs_subject} subject
      JOIN {node__field_inactive} inactive
      ON (subject.revision_id = inactive.revision_id AND subject.bundle = 'catalog_course')
      LEFT JOIN {hfc_academic_crosswalk} crosswalk
      ON (subject.field_crs_subject_target_id = crosswalk.prefix_code)
      WHERE field_inactive_value <> 1 AND new_dept_code IS NULL
      GROUP BY field_crs_subject_target_id, new_dept_code ORDER BY field_crs_subject_target_id"
    )->fetchCol();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getCourseCrosswalkException($prefix, $number) {
    $result = $this->database->query("
      SELECT * FROM hfc_academic_crosswalk_course_exceptions
      WHERE prefix_code = :prefix_code AND course_number = :course_number",
      [
        ':prefix_code' => $prefix,
        ':course_number' => $number,
      ]
    )->fetchAll();
    return !empty($result) ? reset($result) : NULL;
  }

}
