<?php

namespace Drupal\hfc_catalog_helper\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ContentTypeHelpBlock' block.
 *
 * @Block(
 *  id = "hfc_catalog_content_type_help",
 *  admin_label = @Translation("HFC Catalog Content Type Help"),
 * )
 */
class ContentTypeHelpBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use MessengerTrait;

  /**
   * Defines content types for Required Course Connector targets.
   *
   * @var array
   */
  const RELATED_COURSE_TYPES = [
    'catalog_course',
    'course_master',
    'course_proposal',
    'pseudo_course',
  ];

  /**
   * Defines content types for Related Program Connector targets.
   *
   * @var array
   */
  const RELATED_PROGRAM_TYPES = [
    'catalog_program',
    'program_master',
    'program_proposal',
    'pseudo_program',
  ];

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Drupal\Core\Routing\RouteMatchInterface definition.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $routeMatch;

  /**
   * Stores the Module Handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * Stores the current node.
   *
   * @var \Drupal\node\NodeInterface
   */
  private $original;

  /**
   * Stores the corresponding course or program master.
   *
   * @var \Drupal\node\NodeInterface
   */
  private $master;

  /**
   * Stores the list of related content items.
   *
   * @var array
   */
  private $items = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('module_handler')
    );
  }

  /**
   * Constructs a new ReqCourseUsage object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The Route Match service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    RouteMatchInterface $route_match,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Verify that the required modules are loaded.
    if (!$this->moduleHandler->moduleExists('hfc_related_program')) {
      $this->messenger()->addWarning($this->t('The Content Helper Block requires the HFC Related Program module.'));
      return;
    }
    // Get the node from the current route.
    $node = $this->routeMatch->getParameter('node');
    if (empty($node)) {
      return;
    }

    $this->original = $node;

    $build = [];

    // @todo From ReqCourseUsage block.
    // I can't get this to be stable. If I try to implement any useful caching,
    // then the block just goes blank on every page if a related node changes.
    // $build['#cache']['contexts'] = ['url'];
    // $build['#cache']['tags'] = ['req_course:' . $rqc->id(), 'node_list'];
    $build['#cache']['max-age'] = 0;

    if (empty($node->type->entity)) {
      $this->messenger()->addWarning($this->t('Cannot load related content block info.'));
      return $build;
    }

    $build['type'] = [
      '#prefix' => '<p class="content-type">',
      '#suffix' => '</p>',
    ];

    $type = $node->type->entity->label();
    $prep = in_array(mb_substr($type, 0, 1), ['A', 'E', 'I', 'O', 'U']) ? 'an' : 'a';

    if (!empty($node->field_program_master->entity)) {
      $this->master = $node->field_program_master->entity;
      $path = $this->master->toUrl()->toString();

      $build['#markup'] = $this->t(
        'This is @prep @type. <a href="@path">View Program Master</a>',
        ['@type' => $type, '@prep' => $prep, '@path' => $path]
      );

      $this->getRelatedContent($this->master);
    }
    elseif (!empty($node->field_course_master->entity)) {
      $this->master = $node->field_course_master->entity;
      $path = $this->master->toUrl()->toString();

      $build['#markup'] = $this->t(
        'This is @prep @type. <a href="@path">View Course Master</a>',
        ['@type' => $type, '@prep' => $prep, '@path' => $path]
      );

      $this->getRelatedContent($this->master);
    }
    elseif ($node->getType() == 'pseudo_course') {
      $connector = $this->getCourseConnectorId($node);
      $build['#markup'] = $this->t(
        'This is @prep @type. (Connector ID: @id)',
        [
          '@type' => $type,
          '@prep' => $prep,
          '@id' => $connector ?? 'missing',
        ]
      );
    }
    else {
      $build['#markup'] = $this->t(
        'This is @prep @type.',
        ['@type' => $type, '@prep' => $prep]
      );
    }

    if (!empty($node->field_related_program)) {
      foreach ($node->get('field_related_program') as $item) {
        $this->getRelatedPrograms($item->entity);
      }
    }

    $this->getRelatedContent($node);

    if (!empty($this->items)) {
      ksort($this->items);
      $build['related_links'] = [
        '#theme' => 'item_list',
        '#items' => $this->items,
      ];
    }

    return $build;
  }

  /**
   * Get related content by pointers based on content type.
   *
   * @param \Drupal\node\NodeInterface $target
   *   The node to check for related content.
   */
  private function getRelatedContent(NodeInterface $target) {

    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(FALSE);

    $entity_reference_fields = $query->orConditionGroup()
      ->condition('field_course_master', $target->id())
      ->condition('field_program_master', $target->id())
      ->condition('field_replaced_program', $target->id());

    $query->condition($entity_reference_fields);

    // Do not repeat current content.
    $query->condition('nid', $this->original->id(), '<>');

    // Do not repeat already shown master.
    if (!empty($this->master)) {
      $query->condition('nid', $this->master->id(), '<>');
    }

    $query->condition('status', NodeInterface::PUBLISHED);

    $query->sort('type');
    $query->sort('title');

    $result = $query->execute();

    $this->getProgramConnector($target);

    if (!empty($result)) {
      foreach ($result as $nid) {
        $node = $this->entityTypeManager->getStorage('node')->load($nid);

        // @todo Rather than just excluding, this can be modified
        // to throw warnings about deactivated courses and programs, etc.
        if (
          !($node->getType() == 'course_proposal' && $node->field_proposal_processed->value == 1) &&
          !($node->getType() == 'program_proposal' && $node->field_proposal_processed->value == 1) &&
          !($node->getType() == 'program_master' && $node->field_program_status->value == 'inactive') &&
          !($node->getType() == 'catalog_program' && $node->field_program_status->value == 'inactive')
        ) {
          $this->addRelatedItem($node);
          $this->getProgramConnector($node);
        }
      }
    }
  }

  /**
   * Get related program connector referencing target node.
   *
   * @param \Drupal\node\NodeInterface $target
   *   The node to check for related content.
   */
  private function getProgramConnector(NodeInterface $target) {
    if (!in_array($target->getType(), self::RELATED_PROGRAM_TYPES)) {
      return;
    }

    $query = $this->entityTypeManager->getStorage('related_program')->getQuery()
      ->accessCheck(FALSE);
    switch ($target->getType()) {
      case 'catalog_program':
        $query->condition('catalog_nid', $target->id());
        break;

      case 'program_master':
      case 'pseudo_program':
        $query->condition('master_nid', $target->id());
        break;

      case 'program_proposal':
        $query->condition('proposal_nid', $target->id());
        break;

      default:
        // This should never happen. Exit if it does.
        return;

    }

    $id = $query->execute();
    $id = !empty($id) ? reset($id) : NULL;

    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(FALSE);
    $query->condition('field_related_program', $id);

    // Do not repeat current content.
    $query->condition('nid', $this->original->id(), '<>');

    // Do not repeat already shown master.
    if (!empty($this->master)) {
      $query->condition('nid', $this->master->id(), '<>');
    }

    $query->condition('status', NodeInterface::PUBLISHED);

    $query->sort('type');
    $query->sort('title');

    $nids = $query->execute();

    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    foreach ($nodes as $node) {
      $this->addRelatedItem($node);
      $this->getProgramConnector($node);
    }
  }

  /**
   * Get all related programs for a connector ID.
   *
   * @param Drupal\Core\Entity\EntityInterface $connector
   *   Related program connector to check.
   */
  private function getRelatedPrograms(EntityInterface $connector) {
    foreach (['catalog_nid', 'master_nid', 'proposal_nid'] as $field) {
      $node = $connector->get($field)->entity;
      if (is_object($node)) {
        $this->addRelatedItem($node);
      }
    }
  }

  /**
   * Get the ID of the Required Course Connector.
   */
  private function getCourseConnectorId(NodeInterface $node): ?int {
    $query = $this->entityTypeManager->getStorage('req_course')->getQuery()
      ->accessCheck(FALSE);
    switch ($node->getType()) {
      case 'catalog_course':
        $query->condition('catalog_nid', $node->id());
        break;

      case 'course_master':
      case 'pseudo_course':
        $query->condition('master_nid', $node->id());
        break;

      case 'course_proposal':
        $query->condition('proposal_nid', $node->id());
        break;

      default:
        // This should never happen. Exit if it does.
        return NULL;

    }

    $id = $query->execute();
    return !empty($id) ? reset($id) : NULL;
  }

  /**
   * Get related program connector referencing target node.
   *
   * @param \Drupal\node\NodeInterface $target
   *   The node to check for related content.
   */
  private function getCourseConnector(NodeInterface $target) {
    if (!in_array($target->getType(), self::RELATED_COURSE_TYPES)) {
      return;
    }

    if (!$id = $this->getCourseConnectorId($target)) {
      return;
    }

    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(FALSE);

    $rqcfields = $query->orConditionGroup()
      ->condition('field_crs_replacing', $id)
      ->condition('field_crs_xref', $id)
      ->condition('field_program_admission_list', $id)
      ->condition('field_program_rqcor_list', $id)
      ->condition('field_program_rqsup_list', $id)
      ->condition('field_pseudo_courses', $id)
      ->condition('field_crs_xref', $id);

    $query->condition($rqcfields);

    // Do not repeat current content.
    $query->condition('nid', $this->original->id(), '<>');

    // Do not repeat already shown master.
    if (!empty($this->master)) {
      $query->condition('nid', $this->master->id(), '<>');
    }

    $query->condition('status', NodeInterface::PUBLISHED);

    $query->sort('type');
    $query->sort('title');

    $nids = $query->execute();

    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    foreach ($nodes as $node) {
      $this->addRelatedItem($node);
      $this->getProgramConnector($node);
    }
  }

  /**
   * Add a node to the items list.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to add.
   *
   * @todo Add data classes here to mark inactive programs and courses.
   */
  private function addRelatedItem(NodeInterface $node) {

    // When displaying related content for courses,
    // only allow program masters and active proposals.
    $program_allowed = ['program_master', 'program_sequence'];
    if (
      ($node->getType() == 'program_proposal') &&
      ($node->field_proposal_processed->value !== "1")
    ) {
      $program_allowed[] = 'program_proposal';
    }
    if (
      in_array($this->original->getType(), self::RELATED_COURSE_TYPES) &&
      !in_array($node->getType(), array_merge(self::RELATED_COURSE_TYPES, $program_allowed))
    ) {
      return;
    }

    // Add link to the items array. Using title as the array key
    // provides sorting and eliminates redundant entries.
    $title = "{$node->type->entity->label()}: {$node->label()}";
    $this->items[$title] = Link::fromTextAndUrl($title, $node->toUrl());
  }

}
