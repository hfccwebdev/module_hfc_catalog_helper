<?php

namespace Drupal\hfc_catalog_helper\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'faculty_contact_info' formatter.
 *
 * @FieldFormatter(
 *   id = "faculty_contact_info",
 *   label = @Translation("Faculty contact info"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class FacultyContactInfo extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // Retrieves an array of ReqCourse entities to render.
    $entities = $this->getEntitiesToView($items, $langcode);

    foreach ($entities as $delta => $entity) {
      if (!empty($entity->campus_phone->value)) {
        // I REALLY HATE DRUPAL RIGHT NOW.
        $output[] = $entity->campus_phone->view("campus_phone_formatter")[0][0]["#markup"];
      }
      if (!empty($entity->person_email_addresses->value)) {
        $output[] = Html::escape($entity->person_email_addresses->value);
      }
      if (!empty($entity->campus_building->target_id)) {
        $output[] = Html::escape($entity->campus_building->entity->label());
      }
      if (!empty($entity->campus_office->value)) {
        $output[] = Html::escape('Room: ' . $entity->campus_office->value);
      }
      $elements[$delta] = ['#markup' => Html::escape($entity->label()) . ': ' . implode(", ", $output)];
    }

    return $elements;
  }

}
