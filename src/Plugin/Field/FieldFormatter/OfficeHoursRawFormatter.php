<?php

namespace Drupal\hfc_catalog_helper\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the formatter.
 *
 * @FieldFormatter(
 *   id = "office_hours_raw",
 *   label = @Translation("Office hours raw"),
 *   field_types = {
 *     "office_hours",
 *   }
 * )
 */
class OfficeHoursRawFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $rows = [];
    foreach ($items->getValue() as $key => $item) {
      $rows[$key] = $item;
    }
    if (!empty($rows)) {
      $elements[] = ['#markup' => json_encode($rows)];
    }
    return $elements;
  }

}
