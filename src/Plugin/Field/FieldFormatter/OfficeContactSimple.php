<?php

namespace Drupal\hfc_catalog_helper\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'office_contact_simple' formatter.
 *
 * @FieldFormatter(
 *   id = "office_contact_simple",
 *   label = @Translation("Office contact simple"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class OfficeContactSimple extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    // Retrieves an array of ReqCourse entities to render.
    $nodes = $this->getEntitiesToView($items, $langcode);

    foreach ($nodes as $delta => $node) {

      if (!empty($node->field_phone_number->value)) {
        $output[] = Html::escape($node->field_phone_number->value);
      }
      if (!empty($node->field_email_address->value)) {
        $output[] = Html::escape($node->field_email_address->value);
      }
      if (!empty($node->field_building->target_id)) {
        $output[] = Html::escape($node->field_building->entity->label());
      }
      if (!empty($node->field_office_location->value)) {
        $output[] = Html::escape('Room: ' . $node->field_office_location->value);
      }

      $elements[$delta] = ['#markup' => Html::escape($node->label()) . ': ' . implode(", ", $output)];
    }

    return $elements;
  }

}
