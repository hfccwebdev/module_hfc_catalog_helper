<?php

namespace Drupal\hfc_catalog_helper;

/**
 * Interface NewCourseSectionsInterface.
 */
interface NewCourseSectionsInterface {

  /**
   * Get HANK Subjects options.
   */
  public function getSubjectOpts();

  /**
   * Gets academic term options.
   */
  public function getTermOpts();

  /**
   * View the results.
   */
  public function view();

}
