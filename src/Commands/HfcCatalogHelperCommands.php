<?php

namespace Drupal\hfc_catalog_helper\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\hfc_catalog_helper\CatalogArchiveBuilder;
use Drupal\hfc_catalog_helper\DataIntegrityTools;
use Drupal\hfc_catalog_helper\DatabaseUpdates;
use Drush\Commands\DrushCommands;
use Drupal\hfc_catalog_helper\ConcourseDataFeeds;
use Drupal\hfc_catalog_helper\CatalogContentImportService;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class HfcCatalogHelperCommands extends DrushCommands {

  /**
   * The account switcher service.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * Stores the Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Stores the data integrity tools service object.
   *
   * @var \Drupal\hfc_catalog_helper\DataIntegrityTools
   */
  protected $dataIntegrityTools;

  /**
   * Stores the catalog archive builder service object.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogArchiveBuilder
   */
  protected $catalogArchiveBuilder;

  /**
   * Stores the database updates service object.
   *
   * @var \Drupal\hfc_catalog_helper\DatabaseUpdates
   */
  protected $databaseUpdates;

  /**
   * Stores the Concourse data feeds service object.
   *
   * @var \Drupal\hfc_catalog_helper\ConcourseDataFeeds
   */
  protected $concourseDataFeeds;

  /**
   * Stores the Catalog content import service object.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogContentImportService
   */
  protected $catalogContentImport;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Session\AccountSwitcherInterface $account_switcher
   *   The account switching service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\hfc_catalog_helper\DataIntegrityTools $data_integrity_tools
   *   Catalog Data Integrity Tools.
   * @param \Drupal\hfc_catalog_helper\CatalogArchiveBuilder $hfc_catalog_archive
   *   Catalog Archive Builder Service.
   * @param \Drupal\hfc_catalog_helper\DatabaseUpdates $hfc_database_updates
   *   Catalog Archive Builder Service.
   * @param \Drupal\hfc_catalog_helper\ConcourseDataFeeds $concourse_data_feeds
   *   Concourse Data Feeds Service.
   * @param \Drupal\hfc_catalog_helper\CatalogContentImportService $catalog_content_import
   *   Catalog Content Import Service.
   */
  public function __construct(
    AccountSwitcherInterface $account_switcher,
    EntityTypeManagerInterface $entity_type_manager,
    DataIntegrityTools $data_integrity_tools,
    CatalogArchiveBuilder $hfc_catalog_archive,
    DatabaseUpdates $hfc_database_updates,
    ConcourseDataFeeds $concourse_data_feeds,
    CatalogContentImportService $catalog_content_import
  ) {
    $this->accountSwitcher = $account_switcher;
    $this->entityTypeManager = $entity_type_manager;
    $this->dataIntegrityTools = $data_integrity_tools;
    $this->catalogArchiveBuilder = $hfc_catalog_archive;
    $this->databaseUpdates = $hfc_database_updates;
    $this->concourseDataFeeds = $concourse_data_feeds;
    $this->catalogContentImport = $catalog_content_import;
  }

  /**
   * Generate a report of missing hank_hrper references.
   *
   * @validate-module-enabled hfc_catalog_helper
   *
   * @command staffdir:missing
   * @aliases staffdir-missing
   */
  public function missing() {
    echo $this->dataIntegrityTools->staffdirMissingRefReport();
  }

  /**
   * Generate a report of missing employee credential types.
   *
   * @validate-module-enabled hfc_catalog_helper
   *
   * @command staffdir:missing-credentials
   * @aliases staffdir-missing-credentials
   */
  public function missingCredentials() {
    echo $this->dataIntegrityTools->staffdirMissingCredentialsReport();
  }

  /**
   * Generate a catalog archive.
   *
   * @param string $year
   *   The Catalog Year to export.
   * @param string $target
   *   The target directory to export into.
   * @param array $options
   *   An associative array of options whose values
   *   come from cli, aliases, config, etc.
   *
   * @option nc
   *   No Courses.
   * @option np
   *   No Programs.
   * @option nd
   *   No Employee Directory.
   * @option ns
   *   No Program Sequences.
   * @usage drush catalog-archive 2020-2021 /tmp/archive
   *   Creates a Catalog export in the specified directory.
   *   Please make the target directory before exporting.
   * @validate-module-enabled hfc_catalog_helper
   *
   * @command catalog:archive
   * @aliases catalog-archive
   */
  public function archive($year, $target, array $options = [
    'nc' => FALSE,
    'np' => FALSE,
    'nd' => FALSE,
    'ns' => FALSE,
  ]) {
    $this->catalogArchiveBuilder->generate($year, $target, $options);
  }

  /**
   * Perform a database update.
   *
   * @param string $update
   *   The update method to perform.
   * @param array $options
   *   An associative array of options whose values
   *   come from cli, aliases, config, etc.
   *
   * @option uid
   *   The user id to impersonate for the update.
   * @option bundle
   *   The bundle of the entity type being updated.
   * @option issue
   *   The Web Support issue number associated with this update.
   * @usage drush catalog:update --uid=XX --bundle=name --issue=XXXX updateName
   *   Performs the specified update using the user ID provided.
   *   Bundle and issue are only used for selected updates.
   * @validate-module-enabled hfc_catalog_helper
   *
   * @command catalog:update
   * @aliases catalog-update
   */
  public function catalogUpdate($update, array $options = [
    'uid' => NULL,
    'bundle' => NULL,
    'issue' => NULL,
  ]) {
    if (empty($options['uid'])) {
      $this->output()->writeln('You must specify a valid user ID.');
      return;
    }
    elseif (!method_exists($this->databaseUpdates, $update)) {
      $this->output()->writeln('Unrecognized update specified.');
      return;
    }

    // @see https://www.drupal.org/node/218104
    $uid = $options['uid'];
    $user = $this->entityTypeManager->getStorage('user')->load($uid);
    $this->accountSwitcher->switchTo($user);
    $this->databaseUpdates->runUpdate($update, $options);
    $this->accountSwitcher->switchBack();
  }

  /**
   * Extract Concourse data.
   *
   * @param string $feedName
   *   The Concourse feed name.
   * @param array $options
   *   An associative array of options whose values
   *   come from cli, aliases, config, etc.
   *
   * @option term
   *   Academic term code. Required for Course feed.
   * @option school
   *   Select all prefixes for the given school.
   * @option prefix
   *   Course prefix for filtering on course-related extracts.
   * @option number
   *   Course number for filtering on course-related extracts.
   * @usage drush concourse:extract feedName
   *   Outputs the contents of the specified feed.
   * @validate-module-enabled hfc_catalog_helper
   *
   * @command concourse:extract
   */
  public function extract(
    $feedName,
    array $options = ['term' => NULL, 'school' => NULL, 'prefix' => NULL, 'number' => NULL]
  ) {
    echo $this->concourseDataFeeds->extract($feedName, $options);
  }

  /**
   * Upload Concourse data.
   *
   * @param string $feedName
   *   The Concourse feed name.
   * @param array $options
   *   An associative array of options whose values
   *   come from cli, aliases, config, etc.
   *
   * @option term
   *   Academic term code. Required for Course feed.
   * @option school
   *   Select all prefixes for the given school.
   * @option prefix
   *   Course prefix for filtering on course-related extracts.
   * @option number
   *   Course number for filtering on course-related extracts.
   * @usage drush concourse:upload feedName
   *   Uploads the contents of the specified feed to Concourse.
   * @validate-module-enabled hfc_catalog_helper
   *
   * @command concourse:upload
   */
  public function upload(
    $feedName,
    array $options = ['term' => NULL, 'school' => NULL, 'prefix' => NULL, 'number' => NULL]
  ) {
    $this->concourseDataFeeds->upload($feedName, $options);
  }

  /**
   * Import legacy content.
   *
   * @param string $type
   *   The content type to import.
   * @param array $options
   *   An associative array of options whose values
   *   come from cli, aliases, config, etc.
   *
   * @option limit
   *   Number of entries to import.
   * @option force
   *   Force update.
   * @usage drush catalog:content-import funding_proposal --limit=500
   *   Import content of the specified type.
   * @validate-module-enabled hfc_catalog_helper
   *
   * @command catalog:content-import
   */
  public function importContent(string $type, array $options = [
    'limit' => 0,
  ]) {
    $this->catalogContentImport->import($type, $options);
  }

}
