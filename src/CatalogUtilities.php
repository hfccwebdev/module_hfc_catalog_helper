<?php

namespace Drupal\hfc_catalog_helper;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\Sql\Query;
use Drupal\Core\Language\Language;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hankdata\HankDataInterface;
use Drupal\hfc_hank_api\HfcHankApiInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Class CatalogUtilities.
 *
 * Utilities for Catalog.
 *
 * @ingroup hfcc_modules
 */
class CatalogUtilities implements CatalogUtilitiesInterface {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Stores the HANK Data Service.
   *
   * @var \Drupal\hankdata\HankDataInterface
   */
  private $hankData;

  /**
   * Drupal\hfc_hank_api\HfcHankApiInterface definition.
   *
   * @var \Drupal\hfc_hank_api\HfcHankApiInterface
   */
  protected $hankApi;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private $currentUser;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   * @param \Drupal\hankdata\HankDataInterface $hankdata
   *   The HANK Data service.
   * @param \Drupal\hfc_hank_api\HfcHankApiInterface $hank_api
   *   The hank api.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(
    Connection $database,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    TimeInterface $time,
    HankDataInterface $hankdata,
    HfcHankApiInterface $hank_api,
    AccountProxyInterface $current_user
  ) {
    $this->database = $database;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
    $this->hankdata = $hankdata;
    $this->hankApi = $hank_api;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeTitlesByType($type) {
    if ($nodes = $this->database->query("SELECT n.nid, d.title FROM {node} n
      JOIN {node_field_data} d ON n.vid = d.vid WHERE d.type = :type ORDER BY d.title
      ", [':type' => $type])->fetchAll()
    ) {
      $output = [];
      foreach ($nodes as $node) {
        $output[$node->nid] = $node->title;
      }
      return $output;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getNidsByType(string $type, array $conditions = []): ?array {

    $type = is_array($type) ? $type : [$type];
    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', $type, "IN")
      ->accessCheck(FALSE)
      ->sort('title')
      ->sort('type')
      ->sort('nid');

    array_walk($conditions, 'self::addConditionsToQuery', $query);

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function isInactiveMaster(NodeInterface $node) {
    return !empty($node->field_inactive->value);
  }

  /**
   * {@inheritdoc}
   */
  public function findMatchingCourseInfo($subject, $number, $type = NULL) {
    $matches = [];

    // Yes, this searches for matches across all content types.
    // That is not a bug.
    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('field_crs_subject', $subject)
      ->condition('field_crs_number', $number)
      ->accessCheck(FALSE);

    if (!empty($type)) {
      $query->condition('type', $type);
    }

    $nids = $query->execute();

    $matches = array_map('\Drupal\node\Entity\Node::load', $nids);

    return $matches;
  }

  /**
   * {@inheritdoc}
   */
  public function findMatchingCourses(string $subject, string $number): array {
    $matches = [];

    $nodes = $this->findMatchingCourseInfo($subject, $number);
    foreach ($nodes as $node) {
      $matches[] = [
        '@type' => $node->getType(),
        '%title' => $node->toLink()->toString(),
      ];
    }

    if ($hank_course = $this->hankdata->getHankCourseByName($subject, $number)) {
      $matches[] = [
        '@type' => 'hank_course',
        '%title' => $hank_course->label() . ': ' . $hank_course->getTitle(),
      ];
    }

    return $matches;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultContactHours(float $credit_hours): float {
    if ($credit_hours < 1) {
      $contact_hours = $credit_hours * 15;
    }
    elseif ($credit_hours == 1) {
      $contact_hours = $credit_hours * 15 + 1;
    }
    else {
      $contact_hours = $credit_hours * 15 + 2;
    }

    return $contact_hours;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupplementalCourseInfo($master) {
    if (is_object($master)) {
      if (!empty($master->field_supplemental_info->target_id)) {
        return $master->field_supplemental_info->entity;
      }
      else {
        $master = $master->id();
      }
    }
    $result = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', 'supplemental_course_info')
      ->condition('field_course_master', $master)
      ->accessCheck(FALSE)
      ->execute();

    if (!empty($result)) {
      $nid = reset($result);
      return Node::load($nid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCatalogCourse($master) {
    if (is_object($master)) {
      $master = $master->id();
    }
    $result = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', 'catalog_course')
      ->condition('field_course_master', $master)
      ->accessCheck(FALSE)
      ->execute();

    if (!empty($result)) {
      $nid = reset($result);
      return Node::load($nid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveCourseProposal($master) {
    if (is_object($master)) {
      $master = $master->id();
    }
    // Issue #7031 by micah: I don't know why storage query won't
    // work for this one like the ones above.
    $result = $this->database->query("SELECT m.entity_id FROM {node__field_course_master} m
      JOIN {node__field_proposal_processed} p ON m.revision_id = p.revision_id
      WHERE field_course_master_target_id = :master AND field_proposal_processed_value = 0 AND m.bundle = 'course_proposal'
    ", [':master' => $master])->fetchCol();

    if (!empty($result)) {
      $nid = reset($result);
      return Node::load($nid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDegreeTypes() {
    $degree_types = &drupal_static(__FUNCTION__);
    if (!isset($degree_types)) {
      $program_fields = $this->entityFieldManager->getFieldDefinitions('node', 'program_master');
      $degree_types = $program_fields['field_program_type']->getSetting('allowed_values');
    }
    return $degree_types;
  }

  /**
   * {@inheritdoc}
   */
  public function getSchools() {
    $schools = &drupal_static(__FUNCTION__);
    if (!isset($schools)) {
      $program_fields = $this->entityFieldManager->getFieldDefinitions('node', 'program_master');
      $schools = $program_fields['field_school']->getSetting('allowed_values');
    }
    return $schools;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupplementalProgramInfo($master) {
    if (is_object($master)) {
      if (!empty($master->field_supplemental_info->target_id)) {
        return $master->field_supplemental_info->entity;
      }
      else {
        $master = $master->id();
      }
    }
    $result = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', 'supplemental_program_info')
      ->condition('field_program_master', $master)
      ->accessCheck(FALSE)
      ->execute();

    if (!empty($result)) {
      $nid = reset($result);
      return Node::load($nid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCatalogProgram($master) {
    if (is_object($master)) {
      $master = $master->id();
    }
    $result = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', 'catalog_program')
      ->condition('field_program_master', $master)
      ->accessCheck(FALSE)
      ->execute();

    if (!empty($result)) {
      $nid = reset($result);
      return Node::load($nid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveProgramProposal($master) {
    if (is_object($master)) {
      $master = $master->id();
    }
    // Issue #7031 by micah: I don't know why storage query
    // won't work for this one like the ones above.
    $result = $this->database->query("SELECT m.entity_id FROM {node__field_program_master} m
      JOIN {node__field_proposal_processed} p ON m.revision_id = p.revision_id
      WHERE field_program_master_target_id = :master AND field_proposal_processed_value = 0 AND m.bundle = 'program_proposal'
    ", [':master' => $master])->fetchCol();

    if (!empty($result)) {
      $nid = reset($result);
      return Node::load($nid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getHankApiCourse($name) {
    $result = $this->hankApi->getData("courses", $name);
    if (!empty($result)) {
      return reset($result);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getInstrMethods($show_key = FALSE) {
    if ($results = $this->hankApi->getData("instr-methods")) {
      $output = [];
      foreach ($results as $result) {
        if (!preg_match('/DO NOT USE/', $result->INM_DESC)) {
          if ($show_key) {
            $output[$result->INSTR_METHODS_ID] = '(' . $result->INSTR_METHODS_ID . ') ' . $result->INM_DESC;
          }
          else {
            $output[$result->INSTR_METHODS_ID] = $result->INM_DESC;
          }
        }
      }
      return $output;
    }
  }

  /**
   * Expand conditions into the getNidsByType query.
   *
   * @param string[] $condition
   *   An array of optional conditions for filtering.
   *   Array item keys should be field, value, and (optionally) operator.
   * @param varies $index
   *   The index of the array item.
   * @param Drupal\Core\Entity\Query\Sql\Query $query
   *   The query to modify.
   */
  private function addConditionsToQuery(array $condition, $index, Query $query) {
    if (isset($condition['field']) && isset($condition['value'])) {
      if ($condition['value'] == 'exists') {
        $query->exists($condition['field']);
      }
      elseif ($condition['value'] == 'notExists') {
        $query->notExists($condition['field']);
      }
      else {
        $operator = $condition['operator'] ?? '=';
        $query->condition($condition['field'], $condition['value'], $operator ?: NULL);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAcademicLevels() {
    $field_acad_level = &drupal_static(__FUNCTION__);
    if (!isset($field_acad_level)) {
      $program_fields = $this->entityFieldManager->getFieldDefinitions('node', 'program_master');
      $field_acad_level = $program_fields['field_acad_level']->getSetting('allowed_values');
    }
    return $field_acad_level;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelatedArticulationAgreements(NodeInterface $target) {
    $output = [];
    $now = $this->time->getRequestTime();

    // Get all articulation agreement and pseudo program nodes for this target.
    $nodes = $this->getRelatedArticulationAgreementContent($target);

    // Process all pseudo programs found and add to results array.
    foreach ($nodes as $node) {
      if ($node->getType() == 'pseudo_program') {
        $linked = $this->getRelatedArticulationAgreementContent($node);
        $nodes = $nodes + $linked;
      }
    }

    // Display related articulation agreements for all matching nodes.
    foreach ($nodes as $node) {
      if ($node->getType() == 'articulation_agreement') {

        $external_program_name = $node->field_artic_external_program->value;
        $agreement_type = $node->field_artic_agreement_type->value;
        $institution = $node->field_artic_college->entity->label();

        if (
          !empty($node->field_transfer_grid_file->target_id) &&
          !empty($node->field_artic_xfer_grid_date->value)
        ) {
          $file = $node->field_transfer_grid_file->entity;
        }
        elseif (!empty($node->field_artic_agreement_document->target_id)) {
          $file = $node->field_artic_agreement_document->entity;
        }
        else {
          $file = NULL;
        }

        // Check articulation agreement expiration date.
        // Sorry for the double negatives here!
        // And for the ugly date manipulation.
        $expired = !(
          empty($node->field_expiration_date->value) ||
          strtotime($node->field_expiration_date->value) > $now
        );

        if (!empty($file) && !$expired) {
          $url = $file->createFileUrl();
          $external_program = "<a href=\"$url\">$external_program_name</a>";
          $sortkey = "$institution $external_program_name $agreement_type {$node->id()}";
          $output[$sortkey] = ['#markup' => "$external_program $agreement_type agreement with <strong>$institution</strong>"];
        }
      }
    }

    ksort($output);

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function makeNewContent(string $type): NodeInterface {
    $values = [
      'type' => $type,
      'changed' => $this->time->getRequestTime(),
      'status' => NodeInterface::PUBLISHED,
      'language' => Language::LANGCODE_NOT_SPECIFIED,
      'uid' => $this->currentUser->id(),
      'name' => $this->currentUser->getAccountName(),
    ];

    return $this->entityTypeManager->getStorage('node')->create($values);
  }

  /**
   * Execute queries to load related Articulation Agreement content for a node.
   *
   * @param \Drupal\node\NodeInterface $target
   *   The node to search.
   *
   * @return array
   *   An array of matching nodes.
   */
  private function getRelatedArticulationAgreementContent(NodeInterface $target) {

    // Get the related program connector for the target node.
    $query = $this->entityTypeManager->getStorage('related_program')->getQuery()
      ->accessCheck(FALSE);
    switch ($target->getType()) {
      case 'catalog_program':
        $query->condition('catalog_nid', $target->id());
        break;

      case 'program_master':
      case 'pseudo_program':
        $query->condition('master_nid', $target->id());
        break;

      case 'program_proposal':
        $query->condition('proposal_nid', $target->id());
        break;

      default:
        // This should never happen. Exit if it does.
        return [];

    }

    $connector_id = $query->execute();
    $connector_id = !empty($connector_id) ? reset($connector_id) : NULL;

    // Get nodes that reference the related program connector found above.
    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(FALSE);
    $query->condition('field_related_program', $connector_id);
    $query->condition('type', ['articulation_agreement', 'pseudo_program'], 'IN');
    $query->condition('status', NodeInterface::PUBLISHED);

    $nids = $query->execute();

    return !empty($nids)
     ? $this->entityTypeManager->getStorage('node')->loadMultiple($nids)
     : [];
  }

}
