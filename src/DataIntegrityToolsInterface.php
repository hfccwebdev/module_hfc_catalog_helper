<?php

namespace Drupal\hfc_catalog_helper;

/**
 * Defines the Data Integrity Tools interface.
 *
 * @package Drupal\hfc_catalog_helper
 */
interface DataIntegrityToolsInterface {

  /**
   * Create a report of staffdir missing node reference targets.
   */
  public function staffdirMissingRefReport();

  /**
   * Check nodes where staffdir is entity reference target for missing data.
   */
  public function staffdirMissingReferences();

  /**
   * Create a report of missing credentials degree types.
   */
  public function staffdirMissingCredentialsReport();

  /**
   * Compare course contact hours vs Courses Legacy website.
   */
  public function checkLegacyContactHours();

}
