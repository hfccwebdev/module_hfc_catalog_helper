<?php

namespace Drupal\hfc_catalog_helper;

/**
 * Defines the Database Updates Interface.
 */
interface DatabaseUpdatesInterface {

  /**
   * Run an update. This should be called by drush command.
   *
   * @param string $updateName
   *   The update to run.
   * @param array $options
   *   The user ID to impersonate and the bundle of the entity type.
   */
  public function runUpdate(string $updateName, array $options): void;

  /**
   * Verify not running as user 0.
   */
  public function checkUid();

  /**
   * Map subjects to school contact nodes.
   *
   * @see https://dvc.hfcc.net/webadmin/issues/issue5279
   */
  public function subjectSchoolContacts();

  /**
   * Map subjects to schools.
   */
  public function subjectSchools();

}
