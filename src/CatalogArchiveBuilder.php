<?php

namespace Drupal\hfc_catalog_helper;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Utility\Html;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Defines the Catalog Archive Builder Service.
 *
 * @package Drupal\hfc_catalog_helper
 */
class CatalogArchiveBuilder implements CatalogArchiveBuilderInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Stores the Catalog Year.
   *
   * @var string
   */
  protected $catalogYear;

  /**
   * Stores the target base path.
   *
   * @var string
   */
  protected $basePath;

  /**
   * Drupal\Core\Entity\EntityViewBuilderInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $entityViewBuilder;

  /**
   * Stores an array of formatted degree type.
   *
   * @var string[]
   */
  protected $degreeTypes;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Class constructor.
   *
   * @param Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    FileSystemInterface $file_system,
    RendererInterface $renderer
  ) {
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->fileSystem = $file_system;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function generate($catalog_year, $base_path, $options = []) {
    if (!file_exists($base_path) || is_file($base_path)) {
      $this->messenger()->addError(
        $this->t('Target directory @path does not exist! Cannot continue.', ['@path' => $base_path])
      );
      return FALSE;
    }

    $this->catalogYear = $catalog_year;
    $this->basePath = $base_path;
    $this->entityViewBuilder = $this->entityTypeManager->getViewBuilder('node');

    $program_fields = $this->entityFieldManager->getFieldDefinitions('node', 'catalog_program');
    $this->degreeTypes = $program_fields['field_program_type']->getSetting('allowed_values');

    if (!file_exists("$base_path/$catalog_year")) {
      mkdir("$base_path/$catalog_year");
    }

    if (empty($options['nc'])) {
      $this->extractCatalogCourses();
    }
    if (empty($options['np'])) {
      $this->extractCatalogPrograms();
    }
    if (empty($options['ns'])) {
      $this->extractProgramSequences();
    }
    if (empty($options['nd'])) {
      $this->extractCatalogEmployees();
    }

    return TRUE;
  }

  /**
   * Extract Catalog Courses.
   *
   * @return bool
   *   Returns TRUE on successful completion.
   */
  private function extractCatalogCourses() {
    $course_index = [];
    $course_nids = $this->getCatalogCourses();

    if (!file_exists("{$this->basePath}/{$this->catalogYear}/courses")) {
      mkdir("{$this->basePath}/{$this->catalogYear}/courses");
    }

    foreach ($course_nids as $nid) {
      $node = Node::load($nid);
      $url = mb_strtolower(trim($node->field_crs_subject->target_id) . '-' . trim($node->field_crs_number->value));
      echo $url . PHP_EOL;
      $course_index[] = $this->addCourseIndexRow($node, $url);
      $this->entityViewBuilder->resetCache([$node]);
      $output = [
        '#theme' => 'hfc_catalog_archive_wrapper',
        '#page_title' => $this->t(
          'HFC Catalog Archive @year | @title',
          ['@year' => $this->catalogYear, '@title' => $node->label()]
        ),
        '#page_breadcrumbs' => json_encode([
          $this->catalogYear => $this->catalogYear,
          'Courses' => implode('/', [$this->catalogYear, 'courses']),
          Html::escape($node->label()) => implode('/', [$this->catalogYear, 'courses', $url]),
        ]),
        '#page_content' => [
          $this->pageTitle($node->label()),
          $this->entityViewBuilder->view($node, 'catalog_archive'),
        ],
        '#catalog_year' => $this->catalogYear,
      ];
      $contents = $this->renderer->renderRoot($output);
      $filename = implode('/', [$this->basePath, $this->catalogYear, 'courses', $url . '.php']);
      if (!$this->fileSystem->saveData($contents, $filename, FileSystemInterface::EXISTS_REPLACE)) {
        $this->messenger()->addError($this->t('Could not save file @name', ['@name' => $filename]));
        return FALSE;
      }
    }

    $index = [
      '#theme' => 'hfc_catalog_archive_wrapper',
      '#page_title' => $this->t('HFC Catalog Archive @year | Courses', ['@year' => $this->catalogYear]),
      '#page_breadcrumbs' => json_encode([
        $this->catalogYear => $this->catalogYear,
        'Courses' => implode('/', [$this->catalogYear, 'courses']),
      ]),
      '#page_content' => [
        $this->pageTitle('HFC Catalog Courses'),
        [
          '#type' => 'table',
          '#header' => [$this->t('Title'), $this->t('Credit Hours')],
          '#rows' => $course_index,
          '#attributes' => ['summary' => $this->t('A list of available courses.')],
        ],
      ],
      '#catalog_year' => $this->catalogYear,
    ];
    $contents = $this->renderer->renderRoot($index);
    $filename = "{$this->basePath}/{$this->catalogYear}/courses/index.php";
    if (!$this->fileSystem->saveData($contents, $filename, FileSystemInterface::EXISTS_REPLACE)) {
      $this->messenger()->addError($this->t('Could not save file @name', ['@name' => $filename]));
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Builds a row for the Course Index page.
   *
   * @param \Drupal\node\Entity\NodeInterface $node
   *   The node to add.
   * @param string $url
   *   The url for the link target.
   *
   * @return array
   *   A table row array.
   */
  private function addCourseIndexRow(NodeInterface $node, $url) {
    return [
      'data' => [
        $this->t('<a href="@url">@title</a>', [
          '@url' => $url,
          '@title' => $node->label(),
        ]),
        round($node->field_crs_credit_hours->value, 2),
      ],
      'no_striping' => TRUE,
    ];
  }

  /**
   * Extract Catalog Programs.
   */
  private function extractCatalogPrograms() {
    $this->program_index = [];
    $program_nids = $this->getCatalogPrograms();

    if (!file_exists("{$this->basePath}/{$this->catalogYear}/programs")) {
      mkdir("{$this->basePath}/{$this->catalogYear}/programs");
    }

    foreach ($program_nids as $nid) {
      $node = Node::load($nid);
      if (!empty($node->field_program_code->value)) {
        $url = preg_replace("/[^a-z\.]+/", "", mb_strtolower($node->field_program_code->value));
      }
      else {
        $this->messenger()->addWarning($this->t(
          'Missing program code in @nid: @title',
          ['@nid' => $node->id(), '@title' => $node->label()]
        ));
        $url = $node->id();
      }
      echo $url . PHP_EOL;
      $course_index[] = $this->addProgramIndexRow($node, $url);
      $this->entityViewBuilder->resetCache([$node]);
      $output = [
        '#theme' => 'hfc_catalog_archive_wrapper',
        '#page_title' => $this->t(
          'HFC Catalog Archive @year | @title',
          ['@year' => $this->catalogYear, '@title' => $node->label()]
        ),
        '#page_breadcrumbs' => json_encode([
          $this->catalogYear => $this->catalogYear,
          'Programs' => implode('/', [$this->catalogYear, 'programs']),
          Html::escape($node->label()) => implode('/', [$this->catalogYear, 'programs', $url]),
        ]),
        '#page_content' => [
          $this->programPageTitle($node),
          $this->entityViewBuilder->view($node, 'catalog_archive'),
        ],
        '#catalog_year' => $this->catalogYear,
      ];
      $contents = $this->renderer->renderRoot($output);
      $filename = implode('/', [$this->basePath, $this->catalogYear, 'programs', $url . '.php']);
      if (!$this->fileSystem->saveData($contents, $filename, FileSystemInterface::EXISTS_REPLACE)) {
        $this->messenger()->addError($this->t('Could not save file @name', ['@name' => $filename]));
        return FALSE;
      }
    }

    $index = [
      '#theme' => 'hfc_catalog_archive_wrapper',
      '#page_title' => $this->t('HFC Catalog Archive @year | Programs', ['@year' => $this->catalogYear]),
      '#page_breadcrumbs' => json_encode([
        $this->catalogYear => $this->catalogYear,
        'Programs' => implode('/', [$this->catalogYear, 'programs']),
      ]),
      '#page_content' => [
        $this->pageTitle('HFC Catalog Programs'),
        [
          '#type' => 'table',
          '#header' => [$this->t('Title'), $this->t('Minumum Total Credit Hours')],
          '#rows' => $course_index,
          '#attributes' => ['summary' => $this->t('A list of available programs.')],
        ],
      ],
      '#catalog_year' => $this->catalogYear,
    ];
    $contents = $this->renderer->renderRoot($index);
    $filename = "{$this->basePath}/{$this->catalogYear}/programs/index.php";
    if (!$this->fileSystem->saveData($contents, $filename, FileSystemInterface::EXISTS_REPLACE)) {
      $this->messenger()->addError($this->t('Could not save file @name', ['@name' => $filename]));
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Builds a row for the Program Index page.
   *
   * @param \Drupal\node\Entity\NodeInterface $node
   *   The node to add.
   * @param string $url
   *   The relative URL for the link target.
   *
   * @return array
   *   A table row array.
   */
  private function addProgramIndexRow(NodeInterface $node, $url) {
    return [
      'data' => [
        $this->t('<a href="@url">@title</a>', [
          '@url' => $url,
          '@title' => $node->label(),
        ]),
        number_format($node->field_program_total_cred->value, 2),
      ],
      'no_striping' => TRUE,
    ];
  }

  /**
   * Extract Program Sequences.
   */
  private function extractProgramSequences() {
    $this->index = [];
    $nids = $this->getProgramSequences();

    if (!file_exists("{$this->basePath}/{$this->catalogYear}/sequences")) {
      mkdir("{$this->basePath}/{$this->catalogYear}/sequences");
    }

    foreach ($nids as $nid) {
      $node = Node::load($nid);

      if (empty($node->field_related_program->entity)) {
        continue;
      }

      $url = $node->id();
      echo "$url {$node->label()}" . PHP_EOL;
      $course_index[] = $this->addSequenceIndexRow($node, $url);
      $this->entityViewBuilder->resetCache([$node]);
      $output = [
        '#theme' => 'hfc_catalog_archive_wrapper',
        '#page_title' => $this->t(
          'HFC Catalog Archive @year | @title',
          ['@year' => $this->catalogYear, '@title' => $node->label()]
        ),
        '#page_breadcrumbs' => json_encode([
          $this->catalogYear => $this->catalogYear,
          'Program Sequences' => implode('/', [$this->catalogYear, 'sequences']),
          Html::escape($node->label()) => implode('/', [$this->catalogYear, 'sequences', $url]),
        ]),
        '#page_content' => [
          $this->pageTitle($node->label()),
          $this->entityViewBuilder->view($node, 'catalog_archive'),
        ],
        '#catalog_year' => $this->catalogYear,
      ];
      $contents = $this->renderer->renderRoot($output);
      $filename = implode('/', [$this->basePath, $this->catalogYear, 'sequences', $url . '.php']);
      if (!$this->fileSystem->saveData($contents, $filename, FileSystemInterface::EXISTS_REPLACE)) {
        $this->messenger()->addError($this->t('Could not save file @name', ['@name' => $filename]));
        return FALSE;
      }
    }

    $index = [
      '#theme' => 'hfc_catalog_archive_wrapper',
      '#page_title' => $this->t(
        'HFC Catalog Archive @year | Program Sequences',
        ['@year' => $this->catalogYear]
      ),
      '#page_breadcrumbs' => json_encode([
        $this->catalogYear => $this->catalogYear,
        'Program Sequences' => implode('/', [$this->catalogYear, 'sequences']),
      ]),
      '#page_content' => [
        $this->pageTitle('HFC Program Sequences'),
        [
          '#type' => 'table',
          '#header' => [
            $this->t('Title'),
            $this->t('Program Name'),
            $this->t('Degree Type'),
          ],
          '#rows' => $course_index,
          '#attributes' => ['summary' => $this->t('A list of available sequences.')],
        ],
      ],
      '#catalog_year' => $this->catalogYear,
    ];
    $contents = $this->renderer->renderRoot($index);
    $filename = "{$this->basePath}/{$this->catalogYear}/sequences/index.php";
    if (!$this->fileSystem->saveData($contents, $filename, FileSystemInterface::EXISTS_REPLACE)) {
      $this->messenger()->addError($this->t('Could not save file @name', ['@name' => $filename]));
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Builds a row for the Sequence Index page.
   *
   * @param \Drupal\node\Entity\NodeInterface $node
   *   The node to add.
   * @param string $url
   *   The relative URL for the link target.
   *
   * @return array
   *   A table row array.
   */
  private function addSequenceIndexRow(NodeInterface $node, $url) {
    $program = !empty($node->field_related_program->entity->catalog_nid)
      ? $node->field_related_program->entity->getCatalog()
      : NULL;
    return [
      'data' => [
        $this->t('<a href="@url">@title</a>', [
          '@url' => $url,
          '@title' => $node->label(),
        ]),
        !empty($program)
          ? Html::escape($program->field_program_name->value)
          : $this->t('UNKNOWN'),
        !empty($program)
          ? $this->degreeType($program)
          : $this->t('UNKNOWN'),
      ],
      'no_striping' => TRUE,
    ];
  }

  /**
   * Extract Catalog Faculty and administrators.
   */
  private function extractCatalogEmployees() {

    if (!file_exists("{$this->basePath}/{$this->catalogYear}/employee-directory")) {
      mkdir("{$this->basePath}/{$this->catalogYear}/employee-directory");
    }

    $query = $this->database->select('hank_hrper', 'h');
    $query->fields('h', ['hrper_id']);
    $query->condition(
      'h.pospay_bargaining_unit',
      ['XADM', 'XSEC', 'LO71', '1650', 'SSA', 'DSOE'],
      'IN'
    );
    $query->orderBy('h.last_name');
    $query->orderBy('h.first_name');

    $rows = [];

    if ($employees = $query->execute()->fetchAll()) {
      $entity_storage = $this->entityTypeManager->getStorage('staffdir');

      foreach ($employees as $item) {
        $entity = $entity_storage->load($item->hrper_id);

        $row = [
          '#prefix' => '<div class="person">',
          '#suffix' => '</div>' . PHP_EOL,
        ];
        $row[] = [
          '#prefix' => '<div class="fullname">',
          '#markup' => $entity->getName(TRUE),
          '#suffix' => '</div>',
        ];
        $row[] = [
          '#prefix' => '<div class="position">',
          '#markup' => $entity->get('pos_title')->value,
          '#suffix' => '</div>',
        ];
        if ($credentials = $entity->credentials()) {
          $row[] = [
            '#prefix' => '<div class="credentials">',
            '#markup' => $credentials,
            '#suffix' => '</div>',
          ];
        }
        if ($contact = $entity->contactInfo()) {
          $row[] = [
            '#prefix' => '<div class="contact-info">',
            '#markup' => $contact,
            '#suffix' => '</div>',
          ];
        }
        $rows[] = $row;
      }
    }

    $index = [
      '#theme' => 'hfc_catalog_archive_wrapper',
      '#page_title' => $this->t(
        'HFC Catalog Archive @year | HFC Faculty, Staff, and Administrators', ['@year' => $this->catalogYear]
      ),
      '#page_breadcrumbs' => json_encode([
        $this->catalogYear => $this->catalogYear,
        'HFC Faculty, Staff, and Administrators' => implode('/', [$this->catalogYear, 'employee-directory']),
      ]),
      '#page_content' => [$this->pageTitle(
        'HFC Faculty, Staff, and Administrators'),
        ["#markup" => PHP_EOL],
        $rows,
      ],
      '#catalog_year' => $this->catalogYear,
    ];
    $contents = $this->renderer->renderRoot($index);
    $filename = "{$this->basePath}/{$this->catalogYear}/employee-directory/index.php";
    if (!$this->fileSystem->saveData($contents, $filename, FileSystemInterface::EXISTS_REPLACE)) {
      $this->messenger()->addError($this->t('Could not save file @name', ['@name' => $filename]));
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Select nodes of the desired type.
   *
   * @return int[]
   *   An array of nids.
   */
  private function getCatalogCourses() {
    $query = $this->database->select('node', 'n');
    $query->fields('n', ['nid']);
    $query->join('node_field_data', 'd', "n.vid = d.vid");
    $query->condition('d.type', 'catalog_course', '=');
    $query->condition('d.status', Node::PUBLISHED, '=');
    $query->join('node__field_inactive', 'i', "n.vid = i.revision_id");
    $query->condition('i.field_inactive_value', 1, "<>");
    $query->orderBy('d.title');
    if ($nodes = $query->execute()->fetchAll()) {
      $nids = [];
      foreach ($nodes as $item) {
        $nids[] = $item->nid;
      }
      return $nids;
    }
  }

  /**
   * Select nodes of the desired type.
   *
   * @return int[]
   *   An array of nids.
   */
  private function getCatalogPrograms() {
    $query = $this->database->select('node', 'n');
    $query->fields('n', ['nid']);
    $query->join('node_field_data', 'd', "n.vid = d.vid");
    $query->condition('d.type', 'catalog_program', '=');
    $query->condition('d.status', Node::PUBLISHED, '=');
    $query->join('node__field_program_status', 'i', "n.vid = i.revision_id");
    $query->condition('i.field_program_status_value', 'active2', "<>");
    $query->condition('i.field_program_status_value', 'inactive', "<>");
    $query->join('node__field_employer_sponsored', 's', "n.vid = s.revision_id");
    $query->condition('s.field_employer_sponsored_value', 0);
    $query->orderBy('d.title');
    if ($nodes = $query->execute()->fetchAll()) {
      $nids = [];
      foreach ($nodes as $item) {
        $nids[] = $item->nid;
      }
      return $nids;
    }
  }

  /**
   * Select nodes of the desired type.
   *
   * @return int[]
   *   An array of nids.
   */
  private function getProgramSequences() {
    $acyr = mb_substr($this->catalogYear, -4);
    $nids = [];

    $query = $this->database->select('node', 'n');
    $query->join('node_field_data', 'd', "n.vid = d.vid");
    $query->join('node__field_acyr_list', 'y', "n.vid = y.revision_id");
    $query->join('node__field_related_program', 'p', "n.vid = p.revision_id");
    $query->join('related_program', 'r', "p.field_related_program_target_id = r.id");
    $query->condition('d.type', 'program_sequence', '=');
    $query->condition('d.status', Node::PUBLISHED, '=');
    $query->isNotNull('r.catalog_nid');
    $query->fields('n', ['nid']);
    $query->orderBy('d.title');
    $query->orderBy('n.vid', 'DESC');

    if ($nodes = $query->execute()->fetchAll()) {
      foreach ($nodes as $item) {
        $nids[] = $item->nid;
      }
    }

    return $nids;
  }

  /**
   * Generate render array for page title.
   *
   * @param string $title
   *   The renderable array.
   *
   * @return array
   *   A renderable array
   */
  private function pageTitle($title) {
    return [
      '#prefix' => '<h1 class="page-title">',
      '#markup' => $this->t('@title (@year)', ['@title' => $title, '@year' => $this->catalogYear]),
      '#suffix' => '</h1>',
    ];
  }

  /**
   * Generate render array for program page title.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to render.
   *
   * @return array
   *   A renderable array
   */
  private function programPageTitle(NodeInterface $node) {
    return [
      '#prefix' => '<h1 class="page-title">',
      '#markup' => $this->t('@title (@year)', [
        '@title' => $node->label(),
        '@year' => $this->catalogYear,
      ]),
      '#suffix' => '</h1>',
    ];
  }

  /**
   * Returns the formatted degree type of the node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to render.
   *
   * @return string
   *   The formatted degree type.
   */
  private function degreeType(NodeInterface $node) {
    return !empty($this->degreeTypes[$node->field_program_type->value])
        ? $this->degreeTypes[$node->field_program_type->value]
        : $node->field_program_type->value;
  }

}
