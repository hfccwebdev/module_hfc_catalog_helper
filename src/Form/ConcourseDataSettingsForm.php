<?php

namespace Drupal\hfc_catalog_helper\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the Concourse Settings Form.
 */
class ConcourseDataSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'hfc_catalog_helper.concourse_data_settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hfc_catalog_helper_notification_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['destination_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Destination URL'),
      '#default_value' => $config->get('destination_url'),
      '#maxlength' => 248,
      '#required' => TRUE,
    ];
    $form['shared_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shared Secret'),
      '#default_value' => $config->get('shared_secret'),
      '#maxlength' => 248,
      '#required' => TRUE,
    ];
    $form['campus_feed'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Campus Feed'),
      '#default_value' => $config->get('campus_feed'),
      '#required' => TRUE,
    ];
    $form['school_feed'] = [
      '#type' => 'textarea',
      '#title' => $this->t('School Feed'),
      '#default_value' => $config->get('school_feed'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configurations and save.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('destination_url', $form_state->getValue('destination_url'))
      ->set('shared_secret', $form_state->getValue('shared_secret'))
      ->set('campus_feed', $form_state->getValue('campus_feed'))
      ->set('school_feed', $form_state->getValue('school_feed'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
