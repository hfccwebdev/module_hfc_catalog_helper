<?php

namespace Drupal\hfc_catalog_helper\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hfc_catalog_helper\NewCourseSectionsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NewCourseSectionsForm.
 */
class NewCourseSectionsForm extends FormBase {

  /**
   * Drupal\hfc_catalog_helper\NewCourseSectionsInterface definition.
   *
   * @var \Drupal\hfc_catalog_helper\NewCourseSectionsInterface
   */
  protected $newCourseSections;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hfc_new_course_sections')
    );
  }

  /**
   * Constructs a new NewCourseSectionsForm object.
   */
  public function __construct(
    NewCourseSectionsInterface $hfc_new_course_sections
  ) {
    $this->newCourseSections = $hfc_new_course_sections;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'new_course_sections_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['term'] = [
      '#type' => 'select',
      '#title' => $this->t('Term'),
      '#options' => ['' => $this->t('- any -')] + $this->newCourseSections->getTermOpts(),
      '#weight' => '0',
    ];
    $form['course_subject'] = [
      '#type' => 'select',
      '#title' => $this->t('Course Subject'),
      '#options' => ['' => $this->t('- any -')] + $this->newCourseSections->getSubjectOpts(),
      '#weight' => '0',
    ];
    $form['course_number'] = [
      '#type' => 'textfield',
      '#title' => t('Course Number'),
      '#size' => 5,
      '#maxlength' => 5,
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['output'] = [
      '#prefix' => '<div class="results">',
      'results' => $this->newCourseSections->view($form_state->getValues()),
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

}
