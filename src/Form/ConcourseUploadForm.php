<?php

namespace Drupal\hfc_catalog_helper\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\hankdata\HankData;
use Drupal\hfc_catalog_helper\ConcourseDataFeeds;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form before clearing out the examples.
 */
class ConcourseUploadForm extends ConfirmFormBase {

  /**
   * Stores the Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Stores the HANK Data service.
   *
   * @var \Drupal\hankdata\HankData
   */
  private $hankdata;

  /**
   * Stores the Concourse Data Feeds service.
   *
   * @var \Drupal\hfc_catalog_helper\ConcourseDataFeeds
   */
  private $feeds;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('hankdata'),
      $container->get('concourse_data_feeds')
    );
  }

  /**
   * Initialize the object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager service.
   * @param \Drupal\hankdata\HankData $hankdata
   *   The HANK Data service.
   * @param \Drupal\hfc_catalog_helper\ConcourseDataFeeds $concourse_data_feeds
   *   The Concourse Data Feeds service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    HankData $hankdata,
    ConcourseDataFeeds $concourse_data_feeds
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->hankdata = $hankdata;
    $this->feeds = $concourse_data_feeds;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hfc_catalog_helper_concourse_upload';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Upload data feeds to Concourse');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('hfc_catalog_helper.concourse_upload');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Upload to Concourse');
  }

  /**
   * Get the data for the schools feed from configuration.
   *
   * @return array
   *   An options array.
   */
  private function getSchoolOpts() {

    $feed = self::config(ConcourseDataFeeds::SETTINGS)->get('school_feed');
    $feed = explode(PHP_EOL, $feed);
    $schools = [];
    foreach ($feed as $row) {
      [$key, $value] = explode('|', $row);
      $schools[$key] = $value;
    }
    // This value is needed for Concourse, but will not work here.
    unset($schools['Other']);

    return ['' => '- all schools -'] + $schools;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $current_term_id = $this->hankdata->getHankCurrentTerm();
    $current_term = $this->entityTypeManager->getStorage('hank_term')->load($current_term_id);

    $form['instructions'] = [
      '#type' => 'markup',
      '#prefix' => '<div class="form-instructions">',
      '#markup' => $this->t('Please select course or section information to upload to Concourse.'),
      '#suffix' => '</div>',
    ];

    $form['feed_name'] = [
      '#type' => 'radios',
      '#title' => $this->t('Feed Type'),
      '#options' => [
        'Template' => $this->t('Build or update Templates from HFC Course Masters'),
        'Clone' => $this->t('Clone Syllabi for sections from Concourse Templates'),
      ],
      '#required' => TRUE,
    ];

    $form['term'] = [
      '#type' => 'select',
      '#title' => $this->t('Semester'),
      '#options' => $this->hankdata->getHankTermOpts([
        'start_date' => $current_term->term_start_date->value,
      ]),
      '#default_value' => $current_term_id,
      '#required' => TRUE,
    ];

    $form['course-selection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Course selections'),
      '#description' => $this->t('Leave this section blank to upload info for all courses.'),
      '#tree' => FALSE,
    ];

    $form['course-selection']['school'] = [
      "#type" => 'select',
      '#title' => $this->t('School'),
      '#options' => $this->getSchoolOpts(),
      '#states' => [
        'disabled' => [':input[name="prefix"]' => ['!value' => '']],
      ],
    ];

    $form['course-selection']['prefix'] = [
      "#type" => 'select',
      '#title' => $this->t('Prefix'),
      '#options' => ['' => '- all prefixes -'] + $this->hankdata->getHankSubjectOpts(['active' => TRUE]),
      '#states' => [
        'disabled' => [':input[name="school"]' => ['!value' => '']],
      ],
    ];

    $form['course-selection']['number'] = [
      "#type" => 'textfield',
      '#title' => $this->t('Number'),
      '#size' => 10,
      '#states' => [
        'disabled' => [':input[name="school"]' => ['!value' => '']],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $feed_name = $form_state->getValue('feed_name');

    // Set default options first, like the drush command does.
    $options = [
      'term' => $form_state->getValue('term'),
      'school' => NULL,
      'prefix' => NULL,
      'number' => NULL,
    ];

    $school = $form_state->getValue('school') ?? NULL;
    $prefix = $form_state->getValue('prefix') ?? NULL;
    $number = $form_state->getValue('number') ?? NULL;

    if (!empty($school)) {
      $options['school'] = $school;
    }
    elseif (!empty($prefix)) {
      $options['prefix'] = $prefix;
      $options['number'] = $number;
    }

    $batch_builder = (new BatchBuilder())
      ->setTitle($this->t('Concourse upload'))
      ->setFinishCallback('hfc_catalog_helper_concourse_upload_batch_finished');

    // We have to split these up due to execution time.
    if ($feed_name == 'Template') {
      $feeds = [
        'CourseTemplate',
        'SectionTemplate',
        'Description',
        'Objective',
        'Outcome',
      ];
    }
    elseif ($feed_name == 'Clone') {
      $feeds = [
        'Course',
        'Section',
      ];
    }

    foreach ($feeds as $feed) {
      $batch_builder->addOperation(
        'hfc_catalog_helper_concourse_upload_batch_callback',
        [$feed, $options]
      );
    }

    batch_set($batch_builder->toArray());
  }

}
